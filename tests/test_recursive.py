"""
Test recursive.py code
"""

import logging
import os
from unittest.mock import MagicMock, patch

import pytest

from ska_rt.recursive import Recursive


@patch("ska_rt.recursive.git.Repo.clone_from")
@patch("ska_rt.recursive.tempfile.mkdtemp")
def test_clone_repo(mock_mkdtemp, mock_git_clone):
    """
    Test to check if the gitlab repository
    are cloned correctly
    """
    mock_mkdtemp.return_value = "/tests"
    recursive_instance = Recursive()
    mock_mkdtemp.assert_called_once_with(
        dir=recursive_instance.current_directory
    )
    assert recursive_instance.temp_dir == "/tests"

    dep_name = "ska-sdp-config"
    project = "ska-telescope/sdp/ska-sdp-config"
    expected_dir = f"{recursive_instance.temp_dir}/{dep_name}"
    repo_dir = recursive_instance.clone_repo(project, dep_name)
    mock_git_clone.assert_called_once_with(
        f"https://gitlab.com/{project}.git",
        expected_dir,
        branch=None,
        depth=1,
        multi_options=["--recurse-submodules"],
    )
    assert repo_dir == expected_dir


@pytest.mark.parametrize(
    "command, expected_command",
    [
        (["echo", "Hello"], ["echo", "Hello"]),
        ([], ["make", "deps-patch-release"]),
        (None, ["make", "deps-patch-release"]),
    ],
)
@patch("ska_rt.recursive.tempfile.mkdtemp")
def test_run_command(mock_mkdtemp, command, expected_command):
    """Test to check if the command gets run successfully"""
    mock_mkdtemp.return_value = "/tests"
    dep_name = "test"
    recursive_instance = Recursive(command)

    # Mock the subprocess.run method to simulate a successful command execution
    with patch("subprocess.run", return_value=MagicMock()) as mock_subprocess:
        recursive_instance.run_command(dep_name)

        # Verify that subprocess.run was called with the correct arguments
        mock_subprocess.assert_called_once_with(
            expected_command,
            check=True,
            cwd=os.path.join(recursive_instance.temp_dir, dep_name),
        )


def test_invoke():
    """
    Test the invoke method calls run_command
    then deletes the directory
    """
    recursive_instance = Recursive(["echo", "Hello"])
    config_dep = {
        "dep": {
            "project": "test/project",
            "dep_type": "gitlab:python",
            "sink": {},
        }
    }
    with patch.object(
        recursive_instance, "process_repo"
    ) as mock_process_repo, patch.object(
        recursive_instance, "run_command"
    ) as mock_run_command, patch.object(
        recursive_instance, "delete_directory"
    ) as mock_delete_directory:
        recursive_instance.invoke(config_dep)

    mock_process_repo.assert_called_once_with(
        "dep", "test/project", branch=None
    )
    mock_run_command.assert_called_once_with(
        os.path.basename(recursive_instance.current_directory),
        recursive_instance.current_directory,
    )
    mock_delete_directory.assert_called_once()


def test_delete_directory(caplog):
    """Test delete_directory method"""

    recursive_instance = Recursive()
    temp_dir = recursive_instance.temp_dir
    assert os.path.isdir(temp_dir)

    with caplog.at_level(logging.INFO):
        recursive_instance.delete_directory()

    assert not os.path.exists(temp_dir)

    assert f"Temporary Directory Deleted: {temp_dir}" in caplog.text

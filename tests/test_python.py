"""
Test python.py code
"""

from unittest.mock import Mock

import pytest

from src.ska_rt.python import PythonPoetrySink, PythonPoetrySinkURL


@pytest.mark.parametrize("version_only", [True, False])
@pytest.mark.parametrize("have_sources", [True, False])
def test_python_poetry_sink(version_only, have_sources):
    """
    PythonPoetrySink._update_pyproject updates input
    dictionary with correct version info and either
    adds a new source for the version or not,
    depending on what version_only is set to
    and whether the sources exist.
    """
    sink_name = "python:poetry"
    sink_type = "python:poetry"
    sink = PythonPoetrySink(Mock(), sink_name, sink_type)
    sink.version_only = version_only

    tmp_pyproject = {
        "tool": {
            "poetry": {
                "dependencies": {
                    "python": "^3.7",
                    "docopt": "^0.6.2",
                },
                "dev-dependencies": {
                    "black": "^22.1.0",
                },
            }
        }
    }

    if have_sources:
        tmp_pyproject["tool"]["poetry"]["source"] = [
            {"name": "ska", "url": "ska-url"}
        ]

    section = "tool.poetry.dependencies"
    data = {
        "name": "docopt",
        "version": "0.0.0",
        "pypi_url": "secondary-source-url",
    }

    # pylint: disable=protected-access
    sink._update_pyproject(tmp_pyproject, section, data)

    if not version_only:
        assert tmp_pyproject["tool"]["poetry"]["dependencies"]["docopt"] == {
            "version": "==0.0.0",
            "source": "docopt",
        }
        if have_sources:
            assert len(tmp_pyproject["tool"]["poetry"]["source"]) == 2
            assert (
                tmp_pyproject["tool"]["poetry"]["source"][0]["name"] == "ska"
            )
            assert tmp_pyproject["tool"]["poetry"]["source"][1] == {
                "name": "docopt",
                "url": "secondary-source-url",
                "priority": "supplemental",
            }
        else:
            assert len(tmp_pyproject["tool"]["poetry"]["source"]) == 1
            assert tmp_pyproject["tool"]["poetry"]["source"][0] == {
                "name": "docopt",
                "url": "secondary-source-url",
                "priority": "supplemental",
            }

    if version_only:
        assert (
            tmp_pyproject["tool"]["poetry"]["dependencies"]["docopt"]
            == "==0.0.0"
        )
        if have_sources:
            assert len(tmp_pyproject["tool"]["poetry"]["source"]) == 1
            assert (
                tmp_pyproject["tool"]["poetry"]["source"][0]["name"] == "ska"
            )
        else:
            assert "source" not in tmp_pyproject["tool"]["poetry"]


@pytest.mark.parametrize("version_only", [True, False])
def test_python_poetry_sink_extra_info(version_only):
    """
    When a dependency is defined in pyproject.toml with extra information
    already, such as "extras" or "source", this information needs
    to be kept in the updated version as well.

    "source" needs updating if working with development versions,
    but needs to be kept as originally it was if working with
    releases.
    """

    sink_name = "python:poetry"
    sink_type = "python:poetry"
    sink = PythonPoetrySink(Mock(), sink_name, sink_type)
    sink.version_only = version_only

    tmp_pyproject = {
        "tool": {
            "poetry": {
                "dependencies": {
                    "python": "^3.7",
                    "docopt": {
                        "version": "^0.6.2",
                        "extras": ["extra-dep"],
                        "source": "original-source-ref",
                    },
                    "python-gitlab": "^4.12.2",
                }
            }
        }
    }

    section = "tool.poetry.dependencies"
    data = {
        "name": "docopt",
        "version": "0.0.0",
        "pypi_url": "secondary-source-url",
    }

    # pylint: disable=protected-access
    sink._update_pyproject(tmp_pyproject, section, data)

    result_docopt = tmp_pyproject["tool"]["poetry"]["dependencies"]["docopt"]

    # python-gitlab did not change
    assert (
        tmp_pyproject["tool"]["poetry"]["dependencies"]["python-gitlab"]
        == "^4.12.2"
    )

    if version_only:
        assert result_docopt["version"] == "==0.0.0"
        assert result_docopt["source"] == "original-source-ref"
        assert result_docopt["extras"] == ["extra-dep"]

    if not version_only:
        assert result_docopt["version"] == "==0.0.0"
        assert result_docopt["source"] == "docopt"
        assert result_docopt["extras"] == ["extra-dep"]

        assert len(tmp_pyproject["tool"]["poetry"]["source"]) == 1
        assert tmp_pyproject["tool"]["poetry"]["source"][0] == {
            "name": "docopt",
            "url": "secondary-source-url",
            "priority": "supplemental",
        }


def test_python_poetry_sink_url():
    """
    Test that PythonPoetrySinkURL._update_pyproject
    updates input dictionary using the wheel URL
    """
    sink_name = "python:poetry_url"
    sink_type = "python:poetry_url"
    config = Mock()
    config.check_dep_data = Mock()
    sink = PythonPoetrySinkURL(config, sink_name, sink_type)

    tmp_pyproject = {
        "tool": {
            "poetry": {
                "dependencies": {
                    "python": "^3.7",
                },
                "dev-dependencies": {
                    "black": "^22.1.0",
                },
            }
        }
    }
    section = "tool.poetry.dependencies"
    data = {
        "name": "docopt",
        "wheel_url": "https://test.com/packages/docopt-0.0.0.whl",
        "extras": ["extra-dep"],
    }

    # pylint: disable=protected-access
    sink._update_pyproject(tmp_pyproject, section, data)

    result_docopt = tmp_pyproject["tool"]["poetry"]["dependencies"]["docopt"]
    assert result_docopt["url"] == "https://test.com/packages/docopt-0.0.0.whl"
    assert result_docopt["extras"] == ["extra-dep"]

"""
Integration test for the recursive behaviour
"""

from unittest.mock import patch

import pytest

from ska_rt.config import Config
from ska_rt.recursive import Recursive


@pytest.mark.parametrize("mode", [None])
@pytest.mark.parametrize("config_class", [Config])
def test_invoke(config):
    """
    Test the recursive behaviour

    This test actually goes through and clones the
    ska-sdp-integration repository and its chain of dependencies.
    """
    command = ["echo", "Hello"]
    recursive_instance = Recursive(command)

    for dep_name, dep in config.deps.items():
        recursive_instance.process_repo(dep_name, dep["project"])

    # check that a sub-set of processed repos are
    # matching what is expected; sub-set because we
    # don't want to have to update this list every time
    # the integration repository changes
    expected_output = [
        "ska-sdp-config",
        "ska-sdp-proccontrol",
        "ska-sdp-console",
        "ska-sdp-helmdeploy",
        "ska-telmodel",
        "ska-sdp-lmc",
        "ska-sdp-lmc-queue-connector",
        "ska-sdp-qa",
        "ska-sdp",
    ]

    result = recursive_instance.processed_repo
    for repo in expected_output:
        assert repo in result

    # Clean up
    recursive_instance.delete_directory()


@pytest.mark.parametrize("mode", [None])
@pytest.mark.parametrize("config_class", [Config])
def test_invoke_with_branch(config):
    """Test the recursive behaviour when cloning a branch"""
    command = ["echo", "Hello"]
    recursive_instance = Recursive(command)

    with patch("logging.Logger.error") as mock_logging:
        for dep_name, dep in config.deps.items():
            recursive_instance.process_repo(
                dep_name, dep["project"], branch="skart-testing"
            )

    expected_output = [
        "ska-sdp-config",
        "ska-sdp-proccontrol",
        "ska-sdp-console",
        "ska-sdp-helmdeploy",
        "ska-sdp-lmc",
        "ska-sdp",
    ]

    expected_log = (
        "Branch %s does not exist, cloning from main branch",
        "skart-testing",
    )

    result = recursive_instance.processed_repo
    for repo in expected_output:
        assert repo in result
    # none of the repos should have that branch,
    # so the log was called as many times as repos
    assert len(mock_logging.call_args_list) == len(result)
    # the log for all of them is the same
    assert mock_logging.call_args_list[0].args == expected_log

    # Clean up
    recursive_instance.delete_directory()

"""
Test update.py code
"""

import logging

import pytest

from ska_rt.update import (
    json_get,
    json_noisy_merge,
    json_noisy_update,
    json_set,
)


@pytest.mark.parametrize(
    "json_data, path, expected",
    [
        (
            {"tool": {"poetry": {"name": "project"}}},
            "$.tool.poetry.name",
            "project",
        ),
        (
            {"tool": {"poetry": {"name": "project"}}},
            "$.tool.poetry.version",
            None,
        ),
        (
            {"tool": {"poetry": {"name": "project", "version": "0.0.0"}}},
            "$.tool.poetry",
            {"name": "project", "version": "0.0.0"},
        ),
    ],
)
def test_json_get(json_data, path, expected):
    """
    Test that json_get collects json corrrectly
    """

    result = json_get(json_data, path)
    assert result == expected


@pytest.mark.parametrize(
    "json_data, path, value, merge_existing, expected",
    [
        (
            {"tool": {"poetry": {"name": "project"}}},
            "$.tool.poetry.name",
            "hello",
            False,
            {"tool": {"poetry": {"name": "hello"}}},
        ),
        (
            {"tool": {"poetry": {"name": "project"}}},
            "$.tool.poetry",
            {"version": "0.0.0"},
            True,
            {"tool": {"poetry": {"name": "project", "version": "0.0.0"}}},
        ),
    ],
)
def test_json_set(json_data, path, value, merge_existing, expected):
    """
    Test that json_set can set new values
    """
    json_set(json_data, path, value, merge_existing)
    assert json_data == expected


@pytest.mark.parametrize(
    "json_data, path, value, expected",
    [
        (
            {"tool": {"poetry": {"name": "project"}}},
            "$.tool.poetry.name",
            "hello",
            {"tool": {"poetry": {"name": "hello"}}},
        ),
        (
            {"tool": {"poetry": {"name": "project"}}},
            "$.tool.poetry.version",
            "0.0.0",
            {"tool": {"poetry": {"name": "project", "version": "0.0.0"}}},
        ),
        (
            {"tool": {"poetry": {"name": "project"}}},
            "$.tool.poetry",
            {"name": "hello", "version": "0.0.0"},
            {"tool": {"poetry": {"name": "hello", "version": "0.0.0"}}},
        ),
    ],
)
@pytest.mark.parametrize(
    # both functions behave the same way in these tested cases
    "tested_function",
    [json_noisy_update, json_noisy_merge],
)
def test_json_noisy_update_merge(
    tested_function,
    json_data,
    path,
    value,
    expected,
):
    """
    Test json_noisy_update corrrectly updates json
    """
    tested_function("test", json_data, path, value)
    assert json_data == expected


def test_json_noisy_update_remove_key(caplog):
    """
    Test json_noisy_update logs removal warning correctly
    """
    json_data = {"tool": {"poetry": {"name": "project", "version": "0.0.0"}}}
    path = "$.tool.poetry"
    value = {"name": "project"}
    expected = {"tool": {"poetry": {"name": "project"}}}
    with caplog.at_level(logging.WARNING):
        json_noisy_update("test", json_data, path, value)
        assert json_data == expected
        assert "Removing $.tool.poetry.version" in caplog.text


def test_json_noisy_update_warn_if_new(caplog):
    """
    Test warn_if_new creates warning
    """
    json_data = {"tool": {"poetry": {"name": "project"}}}
    path = "$.tool.poetry.version"
    value = "0.0.0"
    expected = {"tool": {"poetry": {"name": "project", "version": "0.0.0"}}}
    with caplog.at_level(logging.WARNING):
        json_noisy_update("test", json_data, path, value, warn_if_new=True)
        assert json_data == expected
        assert "test: Adding new $.tool.poetry.version = 0.0.0" in caplog.text


def test_json_noisy_update_recursive_update(caplog):
    """
    Test correct warnings are logged for recursive updates
    """
    json_data = {"tool": {"poetry": {"name": "project", "version": "0.0.0"}}}
    path = "$.tool.poetry"
    value = {"name": "project_1", "dependencies": "dep_1"}
    expected = {
        "tool": {"poetry": {"name": "project_1", "dependencies": "dep_1"}}
    }
    with caplog.at_level(logging.INFO):
        json_noisy_update("test", json_data, path, value)
        assert json_data == expected
        assert "test: Removing $.tool.poetry.version" in caplog.text
        assert (
            "test: Setting $.tool.poetry.name = project_1 (old: project)"
            in caplog.text
        )
        assert "test: Adding $.tool.poetry.dependencies = dep_1" in caplog.text


def test_json_noisy_merge_recursive_update(caplog):
    """
    The difference between json_noisy_update and json_noisy_merge
    is when the data needs updating recursively from dictionaries.

    This test that path for json_noisy_merge.
    """
    json_data = {"tool": {"poetry": {"name": "project", "version": "0.0.0"}}}
    path = "$.tool.poetry"
    value = {"name": "project_1", "dependencies": "dep_1"}
    expected = {
        "tool": {
            "poetry": {
                "name": "project_1",
                "dependencies": "dep_1",
                "version": "0.0.0",
            }
        }
    }
    with caplog.at_level(logging.INFO):
        json_noisy_merge("test", json_data, path, value)
        assert json_data == expected
        assert "test: Removing $.tool.poetry.version" not in caplog.text
        assert (
            "test: Setting $.tool.poetry.name = project_1 (old: project)"
            in caplog.text
        )
        assert "test: Adding $.tool.poetry.dependencies = dep_1" in caplog.text

# pylint: disable=too-few-public-methods

"""Mock Dependency and Sink classes, for testing"""


class MockDep:
    """
    Mock dependency, used for testing.

    Just uses passed parameters as attributes
    """

    @classmethod
    def dep_type_name(cls):
        """Dependency type name"""
        return "mock-dep"

    # pylint: disable=too-many-arguments
    def __init__(self, dep_name, dep_type, **kwargs):
        self.name = dep_name
        self.dep_type = dep_type
        self.attributes = kwargs
        self.sink = {}


class MockSink:
    """
    Mock dependency sink, used for testing.

    Just stores both configuration and dependency data
    """

    @classmethod
    def sink_type_name(cls):
        """Sink type name"""
        return "mock-sink"

    def __init__(self, config, sink_name, sink_type, **kwargs):
        self.name = sink_name
        self.type = sink_type
        self.config = config
        self.kwargs = kwargs
        self.data = None

    def update(self, data):
        """Replace update with storing data in memory"""
        self.data = data


DEP_TYPES = [MockDep]
SINK_TYPES = [MockSink]

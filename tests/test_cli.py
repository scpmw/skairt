"""CLI tests"""

import logging
import os
from unittest.mock import MagicMock, Mock, patch

import pytest

from ska_rt.cli import (
    cmd_get_or_update,
    cmd_help,
    cmd_recursive,
    cmd_set,
    main,
)
from ska_rt.config import Config
from ska_rt.gitlab import DEP_TYPES as GITLAB_DEP_TYPES
from ska_rt.python import SINK_TYPES as PYTHON_SINK_TYPES
from ska_rt.recursive import Recursive
from ska_rt.yaml import SINK_TYPES as YAML_SINK_TYPES


class MockConfig(Config):
    """
    Mock Config class, only needed to be able to
    get output for CLI tests; it uses all
    the code directly from Config.
    """

    def __init__(self, args, dep_type_map=None, sink_type_map=None):
        super().__init__(
            args, dep_type_map=dep_type_map, sink_type_map=sink_type_map
        )
        self.new_dep = []
        self.new_sink = []

    def make_dep(self, dep_name, dep_config):
        # run the original code as is
        update_deps = super().make_dep(dep_name, dep_config)

        # store results in global variable for further inspection in test
        self.new_dep.append(update_deps)
        return update_deps

    def make_sink(self, dep_name, sink_name, sink_config):
        # run the original code as is
        update_sinks = super().make_sink(dep_name, sink_name, sink_config)

        # store results in global variable for further inspection in test
        self.new_sink.append(update_sinks)
        return update_sinks


@patch("ska_rt.python.GenericPythonPoetrySink._write_data", Mock())
@patch("ska_rt.yaml.YamlSink._write_yaml", Mock())
@patch("builtins.open", MagicMock())
@patch("tomlkit.loads", MagicMock())
@patch("ruamel.yaml.main.YAML.load")
@pytest.mark.parametrize("mode", ["release", None])
@pytest.mark.parametrize("config_class", [MockConfig])
def test_cmd_get_or_update(mock_yaml_load, mode, config):
    """
    Test that the update command of the CLI uses release information
    correctly from the test_skart.toml file.

    The test doesn't mock the connection to GitLab, therefore we
    are specifying an existing package (ska-sdp-config,
    ska-sdp-proccontrol, and ska-sdp chart)
    in test_skart.toml and the test returns existing versions.

    It mocks, however, opening and writing the pyproject.toml
    and yaml files, so there is no need for those files to exist.

    `mode` and `config_class` are the arguments for the `config` fixture

    if mode=release:
    test_skart.toml contains a regex for tag that only looks for
    0.4 minor releases. The last of this was 0.4.5,
    that's what this should return.
    Its sink is also updated to ignore sources, which is propagated
    to the sink attributes (tested for: version_only = True).
    (Same for ska-sdp-proccontrol, and ska-sdp)

    if mode=None:
    When dev. releases are searched, we cannot specify what version base
    we are looking for, therefore it will always return whatever the latest
    actual release was and its dev-version. Hence, we cannot test for
    specific versions, only that it contains the "dev" tag.
    In this case, version_only = False (default)
    (Same for ska-sdp-proccontrol, and ska-sdp)
    """
    oci_content = {"proccontrol": {"image": "test-image", "version": "0.0.0"}}
    chart_content = {
        "dependencies": [
            {
                "name": "ska-sdp",
                "version": "0.12.0",
                "repository": "original-repository",
                "condition": "ska-sdp.enabled",
            }
        ]
    }
    mock_yaml_load.side_effect = [oci_content, chart_content]

    cmd_get_or_update(config, is_get=False, is_update=True)

    # [0] = ska-sdp-config; [1] - ska-sdp-proccontrol; [2] - ska-sdp (chart)
    if mode == "release":
        assert config.new_dep[0].attributes["version"] == "0.4.5"
        assert config.new_sink[0].version_only is True
        assert config.new_dep[1].attributes["version"] == "0.11.5"
        assert config.new_dep[2].attributes["version"] == "0.16.1"
        # yaml sink tests
        assert oci_content["proccontrol"]["image"] == "test-image"
        assert oci_content["proccontrol"]["version"] == "0.11.5"
        # helm yaml sink test
        assert chart_content["dependencies"][0]["version"] == "0.16.1"
        assert (
            chart_content["dependencies"][0]["repository"]
            == "original-repository"
        )

    else:
        assert "dev." in config.new_dep[0].attributes["version"]
        assert config.new_sink[0].version_only is False
        assert "dev." in config.new_dep[1].attributes["version"]
        assert "dev." in config.new_dep[2].attributes["version"]
        # yaml sink tests
        assert oci_content["proccontrol"]["image"] == (
            "registry.gitlab.com/ska-telescope/sdp/"
            "ska-sdp-proccontrol/ska-sdp-proccontrol"
        )
        assert "dev" in oci_content["proccontrol"]["version"]
        # helm yaml sink test
        assert "dev" in chart_content["dependencies"][0]["version"]
        assert (
            "https://gitlab.com/api/"
            in chart_content["dependencies"][0]["repository"]
        )


def test_cmd_get_or_update_invalid_dep():
    """Test cmd_get_or_update with an invalid dependency"""
    dep_config = """
    [dep.invalid-dep]
    dep_type = 'unknown:dep'
    """
    args = {
        "--dep-config": dep_config,
        "get": True,
        "update": False,
    }
    config = Config(args)
    with patch("logging.Logger.error") as mock_logger_error:
        cmd_get_or_update(config, is_get=True, is_update=False)
    mock_logger_error.assert_any_call(
        "Dependency %s has unknown type %s!", "invalid-dep", "unknown:dep"
    )


def test_cmd_get_or_update_empty_config():
    """Test CLI with empty config"""
    dep_config = ""
    args = {
        "--dep-config": dep_config,
        "get": True,
        "update": False,
    }
    dep_types = GITLAB_DEP_TYPES
    sink_types = PYTHON_SINK_TYPES + YAML_SINK_TYPES

    config = MockConfig(args, dep_type_map=dep_types, sink_type_map=sink_types)

    cmd_get_or_update(config, is_get=True, is_update=False)
    assert len(config.deps) == 0


def test_get_no_toml():
    """
    CLI.get returns the correct version for package
    that was input in --dep-config, rather than using
    the toml file.
    """
    dep_config = """
    [dep.ska-sdp-datamodels]
    dep_type = 'gitlab:python'
    project = 'ska-telescope/sdp/ska-sdp-datamodels'
    release.tag = '0.1.\\d'
    """
    toml_file = os.path.dirname(__file__) + "/test_skart.toml"
    args = {
        "--wait": None,
        "--requery": None,
        "--branch": None,
        "--dep-file": toml_file,
        "--gitlab-config": None,
        "--dep-config": dep_config,
        "<dependency>": "ska-sdp-datamodels",
        "--mode": "release",
    }
    dep_types = GITLAB_DEP_TYPES
    sink_types = PYTHON_SINK_TYPES + YAML_SINK_TYPES

    config = MockConfig(args, dep_type_map=dep_types, sink_type_map=sink_types)

    cmd_get_or_update(config, is_get=True, is_update=False)

    # only ska-sdp-datamodels is in config
    assert len(config.new_dep) == 1
    assert config.new_dep[0].attributes["name"] == "ska-sdp-datamodels"
    assert config.new_dep[0].attributes["version"] == "0.1.3"


def test_cmd_help_general(caplog):
    """Test we get general help with no arguments"""
    with caplog.at_level(logging.INFO):
        cmd_help(None)
        assert "General topics:" in caplog.text


def test_cmd_help_auth(caplog):
    """
    Test to ensure the help command returns the correct help string with
    'auth' argument
    """
    with caplog.at_level(logging.INFO):
        cmd_help("auth")
        assert "Authentication with Gitlab is a good idea" in caplog.text


def test_cmd_help_unknown_subject(caplog):
    """Test cmd_help with an unknown subject."""
    with caplog.at_level(logging.WARNING):
        cmd_help("unknown_subject")
        assert "No help subject with name unknown_subject!" in caplog.text


@pytest.mark.parametrize(
    "sink_name, should_exist, expected_exception",
    [
        ("some_sink", True, None),
        ("nonexistent_sink", False, SystemExit),
    ],
)
def test_cmd_set(sink_name, should_exist, expected_exception):
    """Test cmd_set with existing and non-existing sinks."""
    dep_config = """
    [dep.ska-sdp-config]
    dep_type = 'gitlab:python'
    sink = { some_sink = {} }
    """
    args = {
        "<dependency>": "ska-sdp-config",
        "<sink>": sink_name,
        "<data=value>": ["version=1.0.0"],
        "--dep-config": dep_config,
    }
    config = Config(args)

    if expected_exception:
        with pytest.raises(expected_exception):
            with patch("logging.Logger.error") as mock_logger_error:
                cmd_set(config, args)
        if not should_exist:
            mock_logger_error.assert_called_with(
                "Dependency %s has no sink with name %s!",
                "ska-sdp-config",
                sink_name,
            )
    else:
        with patch("ska_rt.config.Config.make_sink") as mock_make_sink:
            mock_sink = MagicMock()
            mock_make_sink.return_value = mock_sink
            cmd_set(config, args)
            # Make sure that make_sink was called with the correct parameters
            mock_make_sink.assert_called_with("ska-sdp-config", sink_name, {})
            # and that the update was called with the correct data
            mock_sink.update.assert_called_with({"version": "1.0.0"})


def test_cmd_recursive():
    """Test cmd_recursive"""

    args = {
        "--branch": "test-branch",
        "<command>": ["echo", "testing ska-rt"],
        "invoke-recursive": True,
        "--dep-config": """
        [dep.dependency1]
        dep_type = 'gitlab:python'
        project = 'ska-telescope/dependency1'
        sink = {}
        
        [dep.dependency2]
        dep_type = 'gitlab:python'
        project = 'ska-telescope/dependency2'
        sink = {}
        """,  # noqa: W293
    }

    dep_types = GITLAB_DEP_TYPES
    sink_types = PYTHON_SINK_TYPES + YAML_SINK_TYPES

    config = MockConfig(args, dep_type_map=dep_types, sink_type_map=sink_types)

    # Make sure that the dependencies have been correctly loaded
    assert "dependency1" in config.deps

    recursive = Recursive(args["<command>"])

    # Check recursive command set correctly
    assert recursive.command == ["echo", "testing ska-rt"]

    with patch.object(recursive, "invoke") as mock_invoke:
        cmd_recursive(config, recursive, args.get("--branch"))

        mock_invoke.assert_called_once_with(config.deps, branch="test-branch")


def test_cmd_recursive_no_deps():
    """Test cmd_recursive when there are no dependencies"""
    dep_config = ""
    args = {
        "--branch": "test-branch",
        "<command>": ["echo", "test"],
        "invoke-recursive": True,
        "--dep-config": dep_config,
    }
    dep_types = GITLAB_DEP_TYPES
    sink_types = PYTHON_SINK_TYPES + YAML_SINK_TYPES

    config = MockConfig(args, dep_type_map=dep_types, sink_type_map=sink_types)

    recursive = Recursive(args["<command>"])
    with patch.object(recursive, "invoke") as mock_invoke:
        cmd_recursive(config, recursive, args.get("--branch"))
        # invoke called with empty deps
        mock_invoke.assert_called_once_with({}, branch="test-branch")


def test_main_help():
    """Test main() with 'help' command."""
    test_args = ["skart", "help"]
    with patch("sys.argv", test_args):
        with patch("ska_rt.cli.cmd_help") as mock_cmd_help:
            with patch("ska_rt.cli.Config") as mock_config:
                result = main()
    mock_cmd_help.assert_called_once_with(None)
    mock_config.assert_not_called()
    assert result == 0


def test_main_get():
    """Test main() with 'get' command."""
    test_args = ["skart", "get", "dependency1"]
    with patch("sys.argv", test_args):
        with patch("ska_rt.cli.cmd_get_or_update") as mock_cmd_get_or_update:
            with patch("ska_rt.cli.Config") as mock_config:
                result = main()
    mock_cmd_get_or_update.assert_called_once()
    mock_config.assert_called_once()
    assert result == 0


def test_main_set():
    """Test main() with 'set' command."""
    test_args = ["skart", "set", "dependency1", "sink"]
    with patch("sys.argv", test_args):
        with patch("ska_rt.cli.cmd_set") as mock_cmd_set:
            with patch("ska_rt.cli.Config") as mock_config:
                result = main()
    mock_cmd_set.assert_called_once()
    mock_config.assert_called_once()
    assert result == 0


def test_main_invalid_command():
    """Test main() with an invalid command."""
    test_args = ["skart", "invalid"]
    with patch("sys.argv", test_args):
        with pytest.raises(SystemExit):
            main()

"""Tests for objects in gitlab.py"""

import pytest

from ska_rt.config import Config
from ska_rt.gitlab import GitlabHelmChartDep, GitlabPythonDep


@pytest.mark.parametrize("mode", ["something"])
@pytest.mark.parametrize("config_class", [Config])
def test_gitlab_python_dep(config):
    """
    GitlabPythonDep correctly uses "tag" information.

    The test doesn't mock the connection to GitLab, therefore
    dep_config contains references to an existing package and
    the returned version also exists.

    `mode` and `config_class` are the arguments for the `config` fixture

    The config object is only used to get access to GitLab,
    it is not used to obtain the regex information from the toml file.
    We manually add the "tag" information to dep_config,
    therefore, "mode" = <anything> will work for this test.
    """
    dep_config = {
        "dep_type": "gitlab:python",
        "project": "ska-telescope/sdp/ska-sdp-config",
        "sink": {"poetry": {"sink_type": "python:poetry"}},
        "release": {"tag": "0.9.\\d"},
        "tag": "0.9.\\d",
    }
    dep_name = "ska-sdp-config"
    res = GitlabPythonDep(config=config, dep_name=dep_name, **dep_config)
    assert res.attributes["version"] == "0.9.1"


@pytest.mark.parametrize(
    "mode, extra_config",
    [
        (
            "release",
            {
                "tag": "0.16.\\d",
                "job": "helm-chart-publish",
                "repository": "CAR",
            },
        ),
        (None, {}),
    ],
)
@pytest.mark.parametrize("config_class", [Config])
def test_gitlab_helm_dep(mode, extra_config, config):
    """
    GitlabHelmChartDep correctly determines the version and
    repository depending on which CI job we use for parce_trace.

    The test doesn't mock the connection to GitLab, therefore
    dep_config contains references to an existing package and
    the returned version also exists.

    `mode` and `config_class` are the arguments for the `config` fixture

    The config object is only used to get access to GitLab,
    it is not used to obtain the regex information from the toml file.
    We manually unpack the release information to dep_config, based
    on whether it is running in "release" mode or not. This is
    parametrized as `extra_config`.
    """
    dep_config = {
        "dep_type": "gitlab:helm-chart",
        "project": "ska-telescope/sdp/ska-sdp-integration",
        "sink": {"yaml": {"sink_type": "yaml"}},
        "release": {
            "tag": "0.16.\\d",
            "job": "helm-chart-publish",
            "repository": "CAR",
        },
    }
    dep_config = {**dep_config, **extra_config}
    dep_name = "ska-sdp"
    res = GitlabHelmChartDep(config=config, dep_name=dep_name, **dep_config)

    if mode == "release":
        assert res.attributes["version"] == "0.16.1"
        assert res.attributes["repository"] == "CAR"

    else:
        assert "dev" in res.attributes["version"]
        assert "https://gitlab.com/api/" in res.attributes["repository"]

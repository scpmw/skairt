"""
Test yaml.py code
"""

from unittest.mock import MagicMock, Mock, patch

from ska_rt.yaml import HelmDependencySink


@patch("builtins.open", MagicMock())
@patch("ska_rt.yaml.YamlSink._write_yaml")
@patch("ruamel.yaml.main.YAML.load")
def test_helm_chart_sink(mock_load, mock_write):
    """
    HelmDependencySink updates only package in
    input data and leaves rest as is.
    """
    sink_name = "helm-chart:dependency"
    sink_type = "helm-chart:dependency"

    contents = {
        "apiVersion": "v2",
        "name": "my-chart",
        "version": "0.1.0",
        "appVersion": "0.1.0",
        "dependencies": [
            {
                "name": "ska-sdp",
                "version": "0.12.0",
                "repository": "original-repository",
                "condition": "ska-sdp.enabled",
            },
            {
                "name": "ska-tango-base",
                "version": "0.4.6",
                "repository": "original-tango-repository",
                "condition": "ska-tango-base.enabled",
            },
        ],
    }
    mock_load.return_value = contents
    data = {
        "name": "ska-sdp",
        "version": "0.17.0",
        "repository": "new-repository",
    }
    expected = {"version": "0.17.0", "repository": "new-repository"}

    sink = HelmDependencySink(Mock(), sink_name, sink_type, Mock())
    sink.update(data)

    # in sink.update _write_yaml is called with the updated contents
    # which we're testing here
    result = mock_write.call_args_list[0].args[0]

    # 0 is ska-sdp
    assert result["dependencies"][0]["version"] == expected["version"]
    assert result["dependencies"][0]["repository"] == expected["repository"]
    # check that the other doesn't change; 1 is ska-tango-base
    assert result["dependencies"][1]["version"] == "0.4.6"
    assert (
        result["dependencies"][1]["repository"] == "original-tango-repository"
    )

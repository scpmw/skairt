"""Fixtures shared between tests"""

import os

import pytest

from ska_rt.gitlab import DEP_TYPES as GITLAB_DEP_TYPES
from ska_rt.python import SINK_TYPES as PYTHON_SINK_TYPES
from ska_rt.yaml import SINK_TYPES as YAML_SINK_TYPES
from tests.mock import MockDep, MockSink


@pytest.fixture(scope="function", name="config")
def config_fxt(mode, config_class):
    """MockConfig fixture"""
    toml_file = os.path.dirname(__file__) + "/test_skart.toml"
    args = {
        "--dep-file": toml_file,
        "--mode": mode,
    }
    dep_types = GITLAB_DEP_TYPES + [MockDep]
    sink_types = PYTHON_SINK_TYPES + YAML_SINK_TYPES + [MockSink]

    config = config_class(
        args, dep_type_map=dep_types, sink_type_map=sink_types
    )
    return config


@pytest.fixture
def default_args():
    """Default args fixture"""
    return {
        "--wait": None,
        "--requery": None,
        "--branch": None,
        "--dep-file": None,
        "--gitlab-config": None,
        "--dep-config": "[dep]\n",
    }


@pytest.fixture
def default_dep_config():
    """Default dep_config toml fixture"""
    return """
    [dep.test-dep]
    dep_type = 'gitlab:python'
    project = 'some/project'
    """

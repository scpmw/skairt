.. _cli:

``skart`` Command-Line Interface
================================

Command Line Interface: ``skart``


Command - Object matrix
------------------------

This is a table/matrix of the existing commands of ``skart`` and what they can
do with a specific objects.

Commands:

- get
- update
- set
- trigger
- invoke-recursive

Objects:

- dependency (e.g., libraries or packages)
- repository (GitLab repositories)
- branch (Git branches within a repository)
- sink (Target for a specific dependency)
- pipeline (GitLab CI/CD pipelines)


This table lists the various commands available in the ``skart`` CLI tool. 

.. list-table::
   :widths: 25 75
   :header-rows: 1

   * - **Command**
     - **Description**
   * - ``skart [options] get [<dependency>]``
     - Fetch details about a specific dependency or all dependencies if none is specified.
   * - ``skart [options] update [<dependency>]``
     - Update a specific dependency or all dependencies if none is specified.
   * - ``skart [options] set <dependency> <sink> [<data=value>...]``
     - Set the target ``<sink>`` for a specific ``<dependency>``, with optional key-value pairs.
   * - ``skart [options] trigger <repository> [<branch>]``
     - Trigger an operation on a specific ``<repository>``, optionally specifying a branch.
   * - ``skart [options] invoke-recursive [--] [<command>...]``
     - Recursively invoke a shell command across multiple repositories.


This table outlines the various options available for the ``skart`` commands.

.. list-table::
   :widths: 20 80
   :header-rows: 1

   * - **Option**
     - **Description**
   * - ``<dependency>``
     - The specific dependency you want to manage (used in ``get``, ``update``, ``set`` commands).
   * - ``--dep-file <file>``
     - Specify a file listing dependencies (default: ``skart.toml``).
   * - ``--branch <branch>``
     - Use this branch instead of automatically detecting the current branch via Git.
   * - ``--mode <mode>``
     - Use mode information from the given dependency file for package configuration.
   * - ``--gitlab-config=<k=v>``
     - GitLab parameters like ``private_token=...``.
   * - ``--dep-config=<"">``
     - Provide TOML-style direct configuration of dependencies (ignores ``--dep-file`` when used).
   * - ``--wait=<N>``
     - Set the number of seconds to wait for GitLab pipelines to finish before proceeding (default: ``0``).
   * - ``--requery=<N>``
     - Interval (in seconds) to re-check pipeline status while waiting (default: ``5``).
   * - ``-v``
     - Enable verbose logging for more detailed output during execution.


This table summarizes common workflows associated with the ``skart`` CLI.

.. list-table::
   :widths: 30 30 40
   :header-rows: 1

   * - **Workflow**
     - **Command Example**
     - **Description**
   * - Fetch a specific dependency
     - ``skart get <dependency>``
     - Fetches details for the specified dependency.
   * - Fetch all dependencies
     - ``skart get``
     - Retrieves details for all dependencies in the ``skart.toml`` file.
   * - Update a specific dependency
     - ``skart update <dependency>``
     - Updates the specified dependency.
   * - Update all dependencies
     - ``skart update``
     - Updates all dependencies listed in the ``skart.toml`` file.
   * - Set a dependency
     - ``skart set <dependency> <sink> [<data=value>...]``
     - Sets the sink for a dependency with optional key-value pair data.
   * - Trigger a repository build
     - ``skart trigger <repository> [<branch>]``
     - Triggers a build for a repository, optionally specifying the branch to use.
   * - Invoke recursive command
     - ``skart invoke-recursive [--] [<command>...]``
     - Recursively runs a shell command across multiple repositories.
   * - Wait for pipelines to complete
     - ``skart update --wait=60``
     - Waits for up to 60 seconds for running/pending pipelines to complete before updating dependencies.


Usage
-----

.. code-block:: none

    > skart --help

    Manage dependencies between SKA repositories

    Usage:
    skart [options] get [<dependency>]
    skart [options] update [<dependency>]
    skart [options] set <dependency> <sink> [<data=value>...]
    skart [options] help [<subject>]
    skart [options] trigger <repository> [<branch>]
    skart [options] invoke-recursive [--] [<command>...]

    Options:
    <dependency>           The specific dependency you want to manage.
    --dep-file <file>      Specify a file listing dependencies 
                            (default: skart.toml).
    --branch <branch>      Use this branch instead of auto-detecting 
                            the current branch (default: query using git)
    --mode <mode>          Use <mode> information from the specified 
                            dependency file.
    --gitlab-config=<k=v>  Provide GitLab parameters, e.g., private_token=....
    --dep-config=<"">      TOML-style configuration data for dependencies. 
                            If used, the dependency file (--dep-file) is ignored.
    --wait=<N>             Time (in seconds) to wait for GitLab pipelines to 
                            finish before proceeding (default: 0).
    --requery=<N>          Interval (in seconds) to re-check pipeline status 
                            while waiting (default: 5).
    -v                     Enable verbose logging for more detailed output 
                            during execution.

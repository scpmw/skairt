.. _invoke-recursive:

Invoke commands recursively
===========================

The recursive behavior enables you to manage interdependent repositories effectively by searching for dependency 
files (e.g., ``skart.toml``). It recursively looks for dependencies, clones necessary repositories, and executes commands 
in a structured manner. This ensures that commands are executed from the bottom of the dependency tree upwards.

The recursive behavior consists of:

- Automatically finds ``.toml`` files and resolves dependencies.
- Executes commands on repositories without the ``.toml`` file first, moving upwards in the hierarchy.
- Once all dependent repositories have been processed, it executes the same command on the current repository.

Note that technically any bash command can be executed recursively on repositories using
``skart invoke-recursive``, however, running commands that perform dependency updates or related
processes was in mind when this behaviour was developed.

For more details about the available options and their usage, you can run:

.. code-block:: console

   $ skart invoke-recursive --help

This will provide additional context and commands you can use while working with the ``skart`` tool.

.. toctree::
  :maxdepth: 1
  :caption: Recursive use-cases

  recursive_testing
  recursive_patch

.. _basic_guide:

Managing dependencies
=====================

This section describes the basic usage of ``skart`` and how to set up
a repository if one wants to make use of the dependency management
functionality.

``skart`` has been developed to ease and improve continuous integration
between inter-dependent SKA repositories, which have projects in GitLab
and build and release their artefacts using the
`standard CI templates <https://gitlab.com/ska-telescope/templates-repository>`_.
It searches specific CI pipeline jobs for artefact names, therefore it is
important that the repositories we work with are set up accordingly.

Repository Setup
----------------

So what would an inter-repository continuous integration workflow look
like? For every repository, we should:

1. **Double-check whether we could merge repositories:** While
   ``skart`` smooths things out a bit, it has to be stressed that
   wherever possible, related code should be kept in the same
   repository. However, that is clearly not always possible, as
   sometimes separate artefacts need to be produced.

2. **Set up** ``skart.toml``: This should list all dependencies that
   we consider part of the same "mainline". The logic of continuous
   integration suggests that we should rather err towards being
   inclusive here, so the suggestion is to cover all SKA dependencies
   at minimum. For details of what package types are supported and how
   to set them up see the :ref:`skart_file` section.

   This should support a "release" mode (see :ref:`Getting Started <toml_tags>`),
   which only selects tagged versions of dependencies. For example:

   .. code:: toml

        [dep.ska-sdp-config]
        dep_type = 'gitlab:python'
        project = 'ska-telescope/sdp/ska-sdp-config'
        release.tag = '\d+\.\d+\.\d+'

        [dep.ska-sdp-config.sink.poetry]
        sink_type = 'python:poetry'
        release.version_only = true

   This sets up such that for ``release`` mode we only select packages
   with a version tag (``a.b.c``), and does not pull packages from
   GitLab.

3. **Update the Makefile**: if you want to use ``skart`` together with the
   dependency management make targets from
   `SKA CI/CD Makefiles <https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile/>`_,
   you will need the following information added to the ``Makefile`` of your repository:

   .. code::

        include .make/dependencies.mk

        PROJECT_NAME = <gitlab repository slug>
        PROJECT_PATH = ska-telescope/<path to repository>
        ARTEFACT_TYPE = <artefact type: oci or python>

   If your artefact is neither an oci image nor a python package, use something that is
   more relevant, e.g. helm-chart. This will mean, however, that ``skart`` will not wait
   for the pipeline being executed on the given artefact when run with some of the make targets.
   For a list of supported types see the
   `pipelineWait function <https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile/-/blob/master/.make-dependencies-support?ref_type=heads#L35>`_
   in the `SKA CI/CD Makefiles <https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile/>`_ repository.

4. **Set up CI schedules**: ``skart`` can be used to regularly test
   code with updated versions of dependencies. Set up and more details can be found
   in :ref:`scheduled_tests`.

Running ``skart``
-----------------

Once the set up is complete, you can already run ``skart`` (once it is installed).
The ``skart`` CLI is described here: :ref:`cli`.

For example, one can update dependencies locally, based on the ``skart.toml`` file,
by running the following command:

.. code-block:: bash

    skart update

If you want to update with released versions of dependencies, run:

.. code-block:: bash

    skart update --mode release


Branches
--------

A special case is changes that span multiple repositories, such as
when we need to make a change to a dependency in order to make a
feature in another repository work. In development, this can be
managed simply through installing working versions into a shared local
(virtual) environment, but ordinarily things become much more
complicated in GitLab CI: one would have to go through an entire
release process in order to make the change in the dependency able to
flow downstream.

With ``skart`` there is a simpler option: the default ``branch`` to
find packages for in dependencies defaults to the name of the current
branch when executing ``skart update``. This means that when two (or
more) repositories have a branch of the same name, ``skart`` will
prefer packages from those branches over those from the main
branch. This means that one can create branches in all relevant
repositories and have them test against each other in Gitlab CI.

While this could be a helpful tool for complicated integrations, it
has to be stressed that this is not entirely without risk. Clearly if
we merge changes into repositories in the wrong order here there is a
chance to (temporarily) break the mainline build.

.. _recursive_testing:

Testing changes recursively
===========================

This guide provides instructions for using ``skart`` to test updates in dependent ska repositories.

The `deps-update-devel <https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile/-/blob/master/dependencies.mk?ref_type=heads#L112>`_
standard make target is designed to update dependencies with ``skart`` and ``poetry``
on a given (development) branch. If the specified branch already exists, it checks out and pulls that branch.
If not, it creates a new one. The function then updates dependencies. If there are changes,
it commits them and pushes the branch to GitLab. It waits for the CI pipeline
to run, checking for successful execution and potential failures.

``make deps-update-devel`` used together with ``skart invoke-recursive`` gives the unique
functionality of one being able to update several dependent repositories with
a single command, not having to worry about creating branches, manually triggering
dependency updates or pushing any code on every single repository.

.. note::

    If at any point a pipeline fails, the recursive execution is terminated and
    remaining repositories are not updated. You will need to fix the problem
    on the given branch, then restart execution.

Before starting, make sure that every repository that is involved has
the following set up:

- it uses the latest ``.make`` submodule
- its ``Makefile`` contains the following information:

  .. code-block::

    include .make/dependencies.mk

    PROJECT_NAME = <gitlab repository slug>
    PROJECT_PATH = ska-telescope/<path to repository>
    ARTEFACT_TYPE = <artefact type: oci or python>

  If your artefact is neither an oci image nor a python package, use something that is
  more relevant, e.g. helm-chart. This will mean, however, that ``skart`` will not wait
  for the pipeline being executed on the given artefact when run with some of the make
  targets. For a list of supported types see the
  `pipelineWait function <https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile/-/blob/master/.make-dependencies-support?ref_type=heads#L35>`_
  in the `SKA CI/CD Makefiles <https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile/>`_ repository.

Steps for recursive testing of dependent repositories:

1. Ensure all necessary tools and dependencies are installed, including `ska-rt`.

#. Export the environment variable with the branch name that you will use for testing:

   ``export BRANCH_NAME=<your-branch-name>``

   This is used by the ``deps-update-devel`` make target.

#. Run ``skart`` to test the updates:

   ``skart invoke-recursive --branch <your-branch-name> -- make deps-update-devel``

   make sure you specify the branch so that ``skart`` also uses the correct one.

#. If needed, adjust the following environment variables that affect the behavior:

     - ``SKART_REQUERY``: Set the interval (in seconds) to re-check pipeline status.
     - ``SKART_WAIT``: Set the maximum time (in seconds) to wait for pipelines to complete.

   You can override the default values using the following command-line arguments:

     - ``--wait <seconds>``: default is 0s.
     - ``--requery <seconds>``: default is 5s.

#. ``skart`` will wait for the pipelines to complete based on the configured wait and requery intervals.
   Monitor the output for any errors or failures during testing.

#. Iterate: if any errors occurred in any of the CI pipelines, you will need to get those fixed
   before rerunning the ``skart`` command again.

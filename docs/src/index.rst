SKA inter-repository tooling
============================

A command-line tool for managing SKA inter-repository dependencies. The intention
is to simplify the process of propagating and testing packages between repositories
for continuous integration.

Main features:

* Examine GitLab repositories and locate the most current published packages
* Inject references to up-to-date packages into configuration

This tool is meant to be used in concert with the `SKA CI/CD Makefiles
<https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile/>`_. It
especially enables the dependency management targets (see `dependencies.mk
<https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile/-/blob/master/dependencies.mk>`_)

.. toctree::
  :maxdepth: 1
  :caption: Contents:

  getting_started
  motivation
  guide
  skart_toml
  ci_scheduled
  recursive
  cli
  internals

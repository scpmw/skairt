.. _skart_file:

Toml File
=========

To use ``skart`` between repositories, the first step is to identify the types of dependencies involved.
This information is captured in the ``skart.toml`` file
at the top-level of a given repository (an option to use a differently named file exists).
The three types of inter-repository relationships that can exist are described in more
detail here but - as a summary:

* Python packages

  ``skart`` can check for (and if requested update the entry in ``pyprojet.toml``)
  either development or released versions of python packages in other repositories
  referenced in the file ``pyproject.toml``.

* OCI images

  ``skart`` can check for (and if requested update the entry in the file it is
  referenced, e.g. if used in a helm chart, in ``charts/<chart-name>/values.yaml``) either
  development or released versions of OCI (Docker) images from other repositories.

* Sub-charts of a helm chart

  ``skart`` can check for (and if requested update) the
  entry for dependent Helm charts from other repositories
  referenced in the file ``charts/<chart-name>/Chart.yaml``

Working with released dependencies
----------------------------------

Development and release versions of dependencies are treated differently.
When a property in the toml file has the ``release.`` prefix, that means
it is only used when the CLI is called with ``--mode release``. This will
overwrite some of the default behaviour of ``skart`` so that files can be
updated with released versions of dependencies.

Here are some of the ``release`` options that can be set in the toml file:

- ``release.tag``: specifies what tag regular expression to look for in
  a CI job, which then will be used as the version to update with
  (see also: :ref:`Getting Started <toml_tags>` section.).
- ``release.version_only``: only update the version of the dependency, do not
  add a new source (e.g. image or package repository) for it
- ``release.job``: which CI job to search to find the requested release tag
- ``release.updates``: how to update a yaml file when using released dependencies


In more detail
--------------

Python packages
^^^^^^^^^^^^^^^

These are specified by ``dep_type`` of ``"gitlab:python"`` in
the entry in ``skart.toml``. The most used related ``sink_type`` is
``"python:poetry"``, which will update the ``pyproject.toml`` file.

For this example entry in ``pyproject.toml``

.. code:: toml

    [tool.poetry.dependencies]
    ska-telmodel = "1.15.1"

This is the corresponding part of ``skart.toml``

.. code:: toml

    [dep.ska-telmodel]
    dep_type = "gitlab:python"
    project = "ska-telescope/ska-telmodel"
    release.tag = "\\d.\\d.\\d"

    [dep.ska-telmodel.sink.poetry]
    sink_type = "python:poetry"
    release.version_only = true


OCI Image files
^^^^^^^^^^^^^^^

These are specified by ``dep_type`` of ``"gitlab:oci-image"`` in
the entry in ``skart.toml``. The most used related ``sink_type`` is
``"yaml"``, which will update the given yaml file defined in the
``file`` section of the sink.

For this entry in the file ``charts/<chart-name>/values.yaml``

  .. code:: yaml

    console:
      component: sdp-console
      image: artefact.skao.int/ska-sdp-console
      version: 0.7.0

This is the corresponding part of skart.toml

.. code:: toml

    [dep.ska-sdp-console]
    dep_type = "gitlab:oci-image"
    project = "ska-telescope/sdp/ska-sdp-console"
    release.tag = "\\d+\\.\\d+\\.\\d+"
    release.job = "oci-image-publish"

    [dep.ska-sdp-console.sink.values-yaml]
    sink_type = "yaml"
    file = "charts/ska-sdp/values.yaml"
    updates = [
      { path = "console.image", value = "{path}" },
      { path = "console.version", value = "{version}" },
    ]
    release.updates = [
      { path = "console.version", value = "{version}" },
    ]

Helm Chart dependencies
^^^^^^^^^^^^^^^^^^^^^^^

These are specified by ``dep_type`` of ``"gitlab:helm-chart"`` in
the entry in ``skart.toml``. The most used related ``sink_type`` is
``"yaml"``, which will update the given yaml file defined in the
``file`` section of the sink.

For this entry in the file ``charts/<chart-name>/Chart.yaml``

.. code:: yaml

    dependencies:
    - name: ska-tango-base
      version: 0.4.10
      repository: https://artefact.skao.int/repository/helm-internal
      condition: ska-tango-base.enabled

This is the corresponding part of skart.toml

.. code:: toml

    [dep.ska-tango-base]
    dep_type = "gitlab:helm-chart"
    project = "ska-telescope/tango-base"
    release.tag = "\\d+\\.\\d+\\.\\d+"
    release.job = "helm-chart-publish"

    [dep.ska-tango-base.sink.chart-yaml]
    sink_type = "yaml"
    file = "charts/ska-mid-integration/Chart.yaml"
    updates = [
      { path = "dependencies[?name='ska-tango-base'].repository", value = "{repository}" },
      { path = "dependencies[?name='ska-tango-base'].version", value = "{version}" },
    ]
    release.updates = [
      { path = "dependencies[?name='ska-tango-base'].version", value = "{version}" },
    ]

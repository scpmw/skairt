Getting Started
===============

Installation
------------

Install using ``pip`` from the SKAO central artefact repository::

    $ pip install --extra-index-url https://artefact.skao.int/repository/pypi-internal/simple skart

Available releases of the package are described in the repository:
https://gitlab.com/ska-telescope/ska-rt/-/releases

``skart`` command-line interface
--------------------------------

Functionality is provided by the ``skart`` command-line tool::

  $ skart
  Usage:
    skart [options] get [<dependency>]
    skart [options] update [<dependency>]
    skart [options] set <dependency> <sink> [<data=value>...]
    skart [options] help [<subject>]
    skart [options] trigger <repository> [<branch>]
    skart [options] invoke-recursive [--] [<command>...]


See the complete description of the command-line arguments and options here: :ref:`cli`

Basic concept: Finding packages
-------------------------------

``skart`` uses the basic concept of finding package
versions (released or development) from GitLab CI jobs,
which it uses to update dependency files.

The ``skart`` tool is configured using a configuration file in ``toml`` format
(by default, it uses ``skart.toml``).
This is more fully described in :ref:`skart_file`. As a simple
example, we might create one that reads:

.. code:: toml

    [dep.ska-sdp-config]
    dep_type = 'gitlab:python'
    project = 'ska-telescope/sdp/ska-sdp-config'

And then execute in the same directory::
  
    $ skart get
    Not authenticated to Gitlab API! Invoke with 'help auth' for more information.
    ska-sdp-config: Selected branch master
    ska-sdp-config: Found ska-sdp-config version 0.9.0+dev.cce9fb16c
    ska-sdp-config: Build age is 10:27:49.550728
    [...]

Here the tool has correctly identified version `0.9.0+dev.cce9fb16c`
as the most recent Python package that has been built for the
``master`` branch of the ``ska-telescope/sdp/ska-sdp-config``
repository. We could add ``branch = 'my-branch'`` to ``skart.toml``
to find the newest build from that branch instead.

.. _toml_tags:

Tags and releases
^^^^^^^^^^^^^^^^^

While finding the most bleeding-edge package from the repository is
useful in some situations, sometimes we would rather find a build
associated with a Git tag, such as a release.  We can specify the tag
to select as follows:

.. code:: toml

    [dep.ska-sdp-config]
    dep_type = 'gitlab:python'
    project = 'ska-telescope/sdp/ska-sdp-config'
    tag = '0\.8\.\d+'

The ``tag`` specification is a regular expression, so we are looking
for a Git tag that matches ``0.8.x``.  This yields the following
behaviour::

    $ skart get
    ska-sdp-config: Selected tag 0.8.0 from 2024-08-05T14:45:52.000+01:00
    ska-sdp-config: Found ska-sdp-config version 0.8.0
    ska-sdp-config: Build age is 31 days, 22:27:53.120978
    [...]

This again correctly picked the newest tag matching our specification,
in this case ``0.8.0``, which is over a month old. We could use ``tag
= '\d+\.\d+\.\d+'`` to select for `any` version tag.

Other artefact types
^^^^^^^^^^^^^^^^^^^^

The example above is specifically to find python packages.
However, ``skart`` is able to work with other artefact types
too, such as OCI images and Helm Charts.

An example set up containing the three different artefact
types can be found in the ``tests`` directory of the
repository:
https://gitlab.com/ska-telescope/ska-rt/-/blob/master/tests/test_skart.toml

Also check :py:mod:`ska_rt.gitlab` for supported dependency types.

User guide
----------

What can the ``skart`` CLI be used for?

- Quick update of dependencies with either development
  or released versions: :ref:`basic_guide`.
- Regular (scheduled) testing of code with most recent
  (development or released) dependencies: :ref:`scheduled_tests`
- Execute bash commands on inter-dependent repositories,
  one at a time, working its way up the dependency tree: :ref:`invoke-recursive`
- In combination with
  `SKA CI/CD Makefiles <https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile/>`_:

    - recursive dependency update (development or released versions) of dependent repositories
      with ``skart`` and ``poetry``: :ref:`recursive_testing`
    - recursive patch release of dependent repositories, including dependency update
      (released version only) with ``skart`` and ``poetry``: :ref:`recursive_patch_release`
    - scheduled testing of dependent repositories with most resent dependencies: :ref:`scheduled_tests`

.. note::

    For any behaviour of ``skart`` that is used
    in conjunction with `SKA CI/CD Makefiles <https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile/>`_,
    you need to make sure that the repository(s) in question uses the
    latest version of the submodule.

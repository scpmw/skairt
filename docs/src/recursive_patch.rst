.. _recursive_patch_release:

Performing a Patch Release
==========================

The workflow for making releases is a bit different from the
development workflow because we want to make sure that the package
(and its dependencies) stay permanently available. In particular, we want
to make a permanent record of all of this in the Git repository so
reproducibility is assured.

We would want the release to correspond as closely as
possible to what we actually tested in GitLab CI. However, with the
:ref:`skart-update <scheduled_tests>` GitLab CI stage, we might be using packages of
dependencies that would depend on updated versions of nested
dependencies themselves. This makes this a :ref:`recursive problem <invoke-recursive>`: Any
release of a repository will require releases of basically all
dependency repositories.

.. warning::

    Before you start: Check whether any dependency repository has outstanding
    changes that would require a major or minor release. If so, this will
    require a release to be made manually.

Using the ``skart invoke-recursive`` command together with the
`deps-patch-release <https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile/-/blob/master/dependencies.mk?ref_type=heads#L151>`_
make target allows for patch releases across interdependent SKA repositories.
By following the outlined steps below, you can manage your dependencies and ensure that your releases are correctly
handled by minimizing errors and streamlining your development workflow.

1. **Ensure ska-rt is Installed:**
   Make sure `ska-rt` is installed on your system.

#. **Command:**
   Use the following command format to invoke the recursive behavior:

   .. code-block:: console

      $ skart [options] invoke-recursive [--] [<command>...]

#. **Specify the Dependency File:**
   Use the ``--dep-file`` option to provide the path to the ``.toml`` file that contains your repository dependencies.

#. **Set Release Mode:**
   Include the ``--mode release`` option to specify that the operation should be conducted in release mode.

#. **Patch Release Command:**
   By default, the command executed is ``make deps-patch-release``. This command will also be executed on the current
   repository along with the cloned ones.

#. **Optional Branch Specification:**
   To target a specific branch of the repositories, use the ``--branch`` option followed by the branch name. If the specified
   branch does not exist, the command will fall back to cloning from the master or main branch.

Here is how you would typically run the command for a patch release:

.. code-block:: console

   $ skart invoke-recursive --dep-file skart.toml --mode release -- make deps-patch-release

Practical Example
-----------------

For example, lets look into the the `ska-sdp-proccontrol <https://gitlab.com/ska-telescope/sdp/ska-sdp-proccontrol>`_
repository. It has a ``skart.toml`` file which is dependent on the
`ska-sdp-config <https://gitlab.com/ska-telescope/sdp/ska-sdp-config>`_ repository.

The process works as follows:

1. The command is invoked to perform the patch release.
2. It first clones the `ska-sdp-config` repository, checks out the relevant branch, and executes the
   update on the ``myproject.toml`` and ``poetry.lock`` files.
3. The changes are pushed to CI and the pipeline is monitored until it succeeds and releases the new version.
4. Once the new version is available, the `ska-sdp-proccontrol` repository updates its required files to use this new version.
5. Finally, it pushes the changes to CI to run the pipeline again, and once the pipeline succeeds, it performs the release.


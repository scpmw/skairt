"""
Dependency sinks for more or less generic YAML configuration files
"""

import logging
import os

from ruamel.yaml import YAML

from .update import json_noisy_update

logger = logging.getLogger(__name__)


class YamlSink:
    """Update a YAML file

    Parameters:

    * ``file``: Path of file to update (e.g. ``"pyproject.yaml"``)
    * ``updates``: List of updates -- each a dictionary with values:

       - ``path``: ``.``-separated JSON path to update (allow data
         substitution)
       - ``value``: Value to set (allows data substituation,
         e.g. ``"{version}"``)

    Required dependency data: Only to cover substitutions
    """

    @classmethod
    def sink_type_name(cls):
        """Dependency sink name, used in configuration files"""
        return "yaml"

    # pylint: disable=too-many-arguments,too-many-positional-arguments
    def __init__(
        self, config, sink_name, sink_type, file, updates, **extra_args
    ):
        assert sink_type == self.sink_type_name()
        config.warn_extra_args(sink_name, self.sink_type_name(), extra_args)
        self.config = config
        self.name = sink_name

        self.file = file
        if isinstance(updates, dict):
            updates = [updates]
        self.updates = updates

    def update(self, data):
        """Updates target YAML file with data from the parameter dictionary."""

        # Load target YAML file
        yaml = YAML()
        with open(self.file, "rb") as yaml_in:
            contents = yaml.load(yaml_in)

        # Apply updates
        for update in self.updates:
            path = update.get("path").format(**data)
            val = update.get("value").format(**data)
            json_noisy_update(self.name, contents, path, val)

        # Write
        self._write_yaml(contents, yaml)

    def _write_yaml(self, contents, yaml):
        os.rename(self.file, self.file + ".old")
        try:
            with open(self.file, "wb") as yaml_out:
                yaml.dump(contents, yaml_out)
            os.remove(self.file + ".old")
        except Exception:
            os.rename(self.file + ".old", self.file)
            raise


class HelmDependencySink(YamlSink):
    """Update a Helm dependency

    Parameters:

    * ``file``: Path of ``Chart.yaml`` to update (e.g.
      ``"charts/ska-mid/Chart.yaml"``)

    Required dependency data:

    * ``name``: Name of dependency / package
    * ``repository``: Helm repository to pull from
    * ``version``: Version of package to use
    """

    @classmethod
    def sink_type_name(cls):
        return "helm-chart:dependency"

    def __init__(self, config, sink_name, sink_type, file, **extra_args):
        assert sink_type == self.sink_type_name()
        config.warn_extra_args(sink_name, self.sink_type_name(), extra_args)

        updates = [
            {
                "path": 'dependencies[?name="{name}"].repository',
                "value": "{repository}",
            },
            {
                "path": 'dependencies[?name="{name}"].version',
                "value": "{version}",
            },
        ]
        super().__init__(config, sink_name, sink_type, file, updates)


SINK_TYPES = [YamlSink, HelmDependencySink]

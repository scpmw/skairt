"""
Manage dependencies between SKA repositories

Usage:
  skart [options] get [<dependency>]
  skart [options] update [<dependency>]
  skart [options] set <dependency> <sink> [<data=value>...]
  skart [options] help [<subject>]
  skart [options] trigger <repository> [<branch>]
  skart [options] invoke-recursive [--] [<command>...]

Options:
  <dependency>           The specific dependency you want to manage.
  --dep-file <file>      Specify a file listing dependencies 
                         (default: skart.toml).
  --branch <branch>      Use this branch instead of auto-detecting 
                         the current branch (default: query using git)
  --mode <mode>          Use <mode> information from the specified 
                         dependency file.
  --gitlab-config=<k=v>  Provide GitLab parameters, e.g., private_token=....
  --dep-config=<"">      TOML-style configuration data for dependencies. 
                         If used, the dependency file (--dep-file) is ignored.
  --wait=<N>             Time (in seconds) to wait for GitLab pipelines to 
                         finish before proceeding (default: 0).
  --requery=<N>          Interval (in seconds) to re-check pipeline status 
                         while waiting (default: 5).
  -v                     Enable verbose logging for more detailed output 
                         during execution.
"""  # noqa:501

import inspect
import logging
import sys

from docopt import docopt

from .config import DEP_TYPE_MAP, SINK_TYPE_MAP, Config
from .recursive import Recursive

logger = logging.getLogger(__name__)


class CustomFormatter(logging.Formatter):
    """
    Adapted from https://stackoverflow.com/questions/384076/
    /how-can-i-color-python-logging-output
    """

    white = "\x1b[1m"
    grey = "\x1b[38;20m"
    yellow = "\x1b[33;20m"
    red = "\x1b[31;20m"
    bold_red = "\x1b[31;1m"
    reset = "\x1b[0m"

    format_normal = "%(message)s"
    format_verbose = (
        "%(asctime)s - %(name)s - %(levelname)s - "
        "%(message)s (%(filename)s:%(lineno)d)"
    )

    def __init__(self, verbose):
        super().__init__()
        format_str = self.format_verbose if verbose else self.format_normal
        self.formatters = {
            logging.DEBUG: logging.Formatter(
                self.grey + format_str + self.reset
            ),
            logging.INFO: logging.Formatter(
                (self.white if verbose else self.grey)
                + format_str
                + self.reset
            ),
            logging.WARNING: logging.Formatter(
                self.yellow + format_str + self.reset
            ),
            logging.ERROR: logging.Formatter(
                self.red + format_str + self.reset
            ),
            logging.CRITICAL: logging.Formatter(
                self.bold_red + format_str + self.reset
            ),
        }

    def format(self, record):
        return self.formatters[record.levelno].format(record)


def main():
    """Main entry point for SKA repository tooling CLI"""

    # Parse arguments
    args = docopt(__doc__, version="SKA dependency manager")

    # Initialise logging (globally)
    chan = logging.StreamHandler()
    chan.setLevel(logging.DEBUG)
    chan.setFormatter(CustomFormatter(args["-v"]))
    logging.basicConfig(
        level=(logging.DEBUG if args["-v"] else logging.INFO),
        handlers=[chan],
    )

    # Looking for help?
    if args["help"]:
        cmd_help(args["<subject>"])
        return 0

    # Configuration needed past this point
    config = Config(args)

    # Looking to get or update a dependency?
    if args["get"] or args["update"]:
        cmd_get_or_update(config, args["get"], args["update"])

    # Set a sink manually?
    elif args["set"]:
        cmd_set(config, args)

    # Invoke recursive command on all dependencies
    elif args["invoke-recursive"]:
        recursive = Recursive(args["<command>"])
        cmd_recursive(config, recursive, args.get("--branch"))

    return 0


def cmd_help(subject):
    """Help CLI command"""

    if subject is None:
        logger.info("General topics:")
        logger.info(" auth - information about Gitlab authentication")
        logger.info("Supported dependency types:")
        for dep_type_name, dep_type in DEP_TYPE_MAP.items():
            doc = inspect.getdoc(dep_type)
            if "\n" in doc:
                doc = doc[: doc.index("\n")]
                logger.info(" %s - %s", dep_type_name, doc)
        logger.info("Supported sink types:")
        for sink_type_name, sink_type in SINK_TYPE_MAP.items():
            doc = inspect.getdoc(sink_type)
            if "\n" in doc:
                doc = doc[: doc.index("\n")]
                logger.info(" %s - %s", sink_type_name, doc)

    elif subject == "auth":
        auth_help_string = """
        Authentication with Gitlab is a good idea - otherwise it is very
        easy to get rate limited, and some features might be unavailable.

        Note that you do *not* need to do this if you call this tool from
        CI scripts, as we will automatically authenticate using the job
        token (CI_JOB_TOKEN environment variable). However, for manual use
        you should get a private token:

            https://gitlab.com/-/profile/personal_access_tokens?name=SKAirt+token&scopes=api

        This token can be set using a number of methods:

        * Invoke with --gitlab-config private_token=<token>
        * Add your token to .python-gitlab.cfg, ~/.python-gitlab.cfg
          or /etc/python-gitlab.cfg. You can customise the location
          using the PYTHON_GITLAB_CFG environment variable.

        The Gitlab configuration file might look as follows:

          [global]
          default = gitlab
          [gitlab]
          url = https://gitlab.com
          private_token = <token>
        """

        for log in inspect.cleandoc(auth_help_string).split("\n"):
            logger.info(log)

    elif subject in DEP_TYPE_MAP:
        logger.info("Dependency type %s:", subject)
        for line in inspect.getdoc(DEP_TYPE_MAP[subject]).split("\n"):
            logger.info(line)

    elif subject in SINK_TYPE_MAP:
        logger.info("Sink type %s:", subject)
        for line in inspect.getdoc(SINK_TYPE_MAP[subject]).split("\n"):
            logger.info(line)
    else:
        logger.warning("No help subject with name %s!", subject)


def cmd_get_or_update(config, is_get, is_update):
    """
    Get / update CLI commands

    Get dependency properties from sources, use sinks to update configuration
    files if updating.
    """

    # Gather dependencies
    for dep_name, dep in config.deps.items():
        # Construct dependency
        dep = config.make_dep(dep_name, dep)
        if dep is None:
            continue

        # Get? Just show what we know
        if is_get:
            for attr_name, attr in sorted(dep.attributes.items()):
                logger.info(" -> %s = %s", attr_name, attr)

        # Update? Forward to sinks
        elif is_update:
            for sink_name, sink in dep.sink.items():
                sink = config.make_sink(dep_name, sink_name, sink)
                if sink is None:
                    continue
                sink.update(dep.attributes)


def cmd_set(config, args):
    """
    Set CLI commands

    Manually use sinks to set dependency properties
    """

    # Gather dependencies
    for dep_name, dep in config.deps.items():
        # Get sink
        if args["<sink>"] not in dep["sink"]:
            logger.error(
                "Dependency %s has no sink with name %s!",
                dep_name,
                args["<sink>"],
            )
            sys.exit(1)
        sink = config.make_sink(
            dep_name, args["<sink>"], dep["sink"][args["<sink>"]]
        )
        if sink is None:
            sys.exit(1)

        # Update with given parameters
        data = {
            arg[: arg.index("=")]: arg[arg.index("=") + 1 :]
            for arg in args["<data=value>"]
        }
        sink.update(data)


def cmd_recursive(config, recursive, branch):
    """
    Invoke recursive command.

    Generates dependency tree from the skart.toml file
    Using the dependency tree, invokes the recursive behaviour

    If branch is given, it will try to clone that branch
    for each repository in the dependency tree.
    """

    recursive.invoke(config.deps, branch=branch)

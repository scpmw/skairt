"""
Dependeny sources - i.e. ways we can depend on a project
"""

import abc
import datetime
import logging
import os.path
import re
import time

import dateutil.parser

logger = logging.getLogger(__name__)


def _select_best(options, preferences, to_check=lambda x: x):
    """Find best option according to prioritised preference list

    :param options: Iterable of options
    :param preferences: Priority list
    :param to_check: Property of options to check
    """

    best_opt = None
    best_score = len(preferences)
    for opt in options:
        prop = to_check(opt)
        if prop not in preferences:
            continue
        score = preferences.index(prop)
        if score < best_score:
            best_opt = opt
            best_score = score
    return best_opt


def _natural_keys(text):
    """
    Used for "natural" sorting with strings including numers (where 10 > 2 and
    1.10 > 1.2)

    Source: https://stackoverflow.com/questions/5967500/
       /how-to-correctly-sort-a-string-with-a-number-inside
    """

    return [
        int(text) if text.isdigit() else text for c in re.split(r"(\d+)", text)
    ]


class GitlabTraceDep(metaclass=abc.ABCMeta):
    """GitLab dependency that extracts information from a job trace"""

    @classmethod
    @abc.abstractmethod
    def dep_type_name(cls):
        """Dependency name, used in configuration files"""

    # pylint: disable=too-many-arguments,too-many-positional-arguments
    def __init__(
        self,
        config,
        dep_name,
        dep_type,
        project,
        branch=("{branch}", "main", "master"),
        tag=None,
        job=("oci-image-build",),
        sink=(),
        **extra_args,
    ):
        # Warn if we have no authentication
        config.auth_warning()

        # Save and normalise parameters
        config.warn_extra_args(dep_name, self.dep_type_name(), extra_args)
        assert dep_type == self.dep_type_name()
        self.name = dep_name
        self.config = config
        self.sink = sink
        if isinstance(branch, str):
            branch = [branch]
        branch = [b.format(branch=config.branch) for b in branch]
        if isinstance(job, str):
            job = [job]

        # Find project
        self.project = config.gitlab.projects.get(project)

        # Iterate candidate GitLab jobs that might have the package in question
        self.attributes = None
        for gl_job, trace in self._find_gitlab_trace(branch, tag, job):
            self.attributes = self.parse_trace(gl_job, trace)
            if self.attributes is not None:
                self.job = gl_job
                self.attributes["job_url"] = gl_job.web_url
                self.attributes["pipeline_url"] = gl_job.pipeline["web_url"]
                self.attributes["commit"] = gl_job.pipeline["sha"]
                # Warn if we are on main branch and build is old
                job_time = dateutil.parser.isoparse(gl_job.finished_at)
                now_time = datetime.datetime.now(datetime.timezone.utc)
                if (
                    gl_job.ref in ["main", "master"]
                    and (now_time - job_time).days > 3
                ):
                    logger.warning(
                        "Main build is %s old! "
                        "Make sure nightly builds are running!",
                        now_time - job_time,
                    )
                else:
                    logger.info(
                        "%s: Build age is %s", self.name, now_time - job_time
                    )
                break
            logger.warning(
                "%s: Trace of job %d not useful, trying next",
                dep_name,
                gl_job.id,
            )

        if self.attributes is None:
            raise RuntimeError(f"{dep_name}: Found no matching job on Gitlab!")
        for attr in sorted(self.attributes):
            logger.debug("%s: %s = %s", dep_name, attr, self.attributes[attr])

    def _find_gitlab_trace(self, branch_names, tag_regexp, job_names):
        """Yields traces of matching jobs.

        :param branch_names: Branch names to allow (priority list)
        :param tag_regexp: Regular expression for tags to look for
        :param job_names: Job names to allow (priority list)
        """

        # If we are asking for a tag, that takes priority
        if tag_regexp is not None:
            yield from self._find_gitlab_trace_by_tag(tag_regexp, job_names)
            return

        # Identify branch to use
        gl_branches = self.project.branches.list(all=True)
        self.branch = _select_best(
            gl_branches, branch_names, lambda branch: branch.name
        )

        # Does it have an associated merge request? Then prefer going
        # by that - if the project runs pipelines on merged commits,
        # this means they won't be associated with the commits in the
        # branch itself.
        gl_mrs = self.project.mergerequests.list(
            source_branch=self.branch.name,
            state="opened",
        )
        for gl_mr in gl_mrs:
            logger.info(
                "%s: Selected merge request !%s for branch %s",
                self.name,
                gl_mr.iid,
                self.branch.name,
            )

            # Find successful pipelines
            for pipeline in self._find_pipelines(
                gl_mr.pipelines, sort_pipelines=True
            ):
                # Find a matching job
                gl_jobs = pipeline.jobs.list(scope="success")
                gl_job = _select_best(gl_jobs, job_names, lambda job: job.name)
                if gl_job is None:
                    logger.warning(
                        "%s: Pipeline %d doesn't have job with name "
                        "[%s], trying next",
                        self.name,
                        pipeline.id,
                        ",".join(job_names),
                    )
                    continue

                # Get trace
                yield gl_job, self._get_trace(gl_job)

        # Get commits of branch
        logger.info("%s: Selected branch %s", self.name, self.branch.name)
        commits = self.project.commits.list(
            iterator=True, ref_name=self.branch.name
        )
        for gl_commit in commits:
            # Find successful pipelines
            for pipeline in self._find_pipelines(
                self.project.pipelines,
                {"sha": gl_commit.id, "ref": self.branch.name},
            ):
                # Find a matching job
                gl_jobs = pipeline.jobs.list(scope="success")
                gl_job = _select_best(gl_jobs, job_names, lambda job: job.name)
                if gl_job is None:
                    logger.warning(
                        "%s: Pipeline %s doesn't have job with name "
                        "[%s], trying next",
                        self.name,
                        pipeline.id,
                        ",".join(job_names),
                    )
                    continue

                # Get trace
                yield gl_job, self._get_trace(gl_job)

    def _find_gitlab_trace_by_tag(self, tag_regexp, job_names):
        """Yields traces of jobs matching tag regular expression.

        :param tag_regexp: Regular expression for tags to look for
        :param job_names: Job names to allow (priority list)
        """

        # Identify existing tags, filter by regexp
        gl_tags = self.project.tags.list()
        tag_re = re.compile(tag_regexp)

        # Filter + sort "naturally"
        gl_filtered_tags = sorted(
            (tag for tag in gl_tags if tag_re.fullmatch(tag.name)),
            key=lambda tag: _natural_keys(tag.name),
            reverse=True,
        )

        for gl_tag in gl_filtered_tags:
            logger.info(
                "%s: Selected tag %s from %s",
                self.name,
                gl_tag.name,
                gl_tag.commit["created_at"],
            )

            for pipeline in self._find_pipelines(
                self.project.pipelines,
                {"sha": gl_tag.commit["id"], "ref": gl_tag.name},
            ):
                # Find a matching job
                gl_jobs = pipeline.jobs.list(scope="success")
                gl_job = _select_best(gl_jobs, job_names, lambda job: job.name)
                if gl_job is None:
                    logger.warning(
                        "%s: Pipeline %s doesn't have job with name "
                        "[%s], trying next",
                        self.name,
                        pipeline.id,
                        ",".join(job_names),
                    )
                    continue

                # Get trace
                yield gl_job, self._get_trace(gl_job)

    def _get_trace(self, gl_job):
        """Query the trace for a Gitlab job."""

        # Preferred option: Do it via the API. This only works if we
        # are authenticated in some way.
        if self.config.gitlab.private_token:
            return self.config.gitlab.http_get(
                f"/projects/{self.project.id}/jobs/{gl_job.id}/trace"
            ).text

        # Otherwise fall back to querying the trace via the web UI
        # (clearly dangerous with respect to request limits!)
        return self.config.gitlab.http_get(
            f"{self.config.gitlab.url}/{self.project.path_with_namespace}"
            f"/-/jobs/{gl_job.id}/raw"
        ).text

    def _find_pipelines(
        self, pipelines, pipelines_list_args=None, sort_pipelines=False
    ):
        """Yield all pipelines of interest.

        This particularly waits for pending or running pipelines, if
        configured.

        :param pipelines: API for listing pipelines (project or merge request)
        :param pipeline_list_args: Extra parameters to pass to API
        :param sort_pipelines: Whether to retrieve all pipelines and sort them
           by ID instead of relying on order returned by the API
        """

        if not pipelines_list_args:
            pipelines_list_args = {}

        # List all pipelines and sort if requested (this is required for merge
        # pipelines, as they might return scheduled pipelines out of order with
        # merge request pipelines)
        if sort_pipelines:
            pip_iter = pipelines.list(all=True, **pipelines_list_args)
            pip_iter = sorted(pip_iter, key=lambda pip: pip.id, reverse=True)
        else:
            pip_iter = pipelines.list(iterator=True, **pipelines_list_args)

        # First check for pending pipelines
        start_time = time.time()
        for pipeline in pip_iter:
            # (Successfully) finished? Always re-query - firstly, this
            # might be merge requests pipelines (which return an
            # incomplete record), and secondly we might have waited
            # for a previous pipeline, and therefore might have
            # out-of-date information.
            requeried = self.project.pipelines.get(pipeline.id)
            if requeried.status in ["success"]:
                yield requeried
                continue

            # Not finished yet?
            if pipeline.status in [
                "created",
                "waiting_for_resource",
                "preparing",
                "pending",
                "running",
            ]:
                logger.info(
                    "%s: Waiting for %s pipeline %s...",
                    self.name,
                    requeried.status,
                    pipeline.id,
                )
                while self.config.pipeline_requery_wait(start_time):
                    requeried = self.project.pipelines.get(pipeline.id)
                    if requeried.status not in [
                        "created",
                        "waiting_for_resource",
                        "preparing",
                        "pending",
                        "running",
                    ]:
                        break

                if requeried.status in ["success"]:
                    yield requeried
                    continue

            # If we get here, we ignored the pipeline
            logger.info(
                "%s: Ignoring %s pipeline %s",
                self.name,
                requeried.status,
                pipeline.id,
            )

    @abc.abstractmethod
    def parse_trace(self, gl_job, trace):
        """
        Abstract method that extracts dependency properties
        from Gitlab job trace in subclasses
        """


class GitlabPythonDep(GitlabTraceDep):
    """Python package with source code on GitLab

    Parameters:

    * ``dep_type``: Should be ``'gitlab:python'``
    * ``project``: Gitlab project name (e.g.
      ``ska-telescope/sdp/ska-sdp-config``)
    * ``branch``: (List of) branch names in priority order
      (e.g. ``[ "{branch}", "main", "master" ]``)
    * ``tag``: (List of) tags in priority order (e.g. ``[ "1.2.1" ]``)
      - overrides ``branch``
    * ``job``: Job name to look for
      (e.g. ``[ 'python-build-for-publication',
      'python-build-for-development' ]``)
    """  # noqa:501

    @classmethod
    def dep_type_name(cls):
        return "gitlab:python"

    def __init__(self, *args, **kwargs):
        if "job" not in kwargs:
            kwargs["job"] = [
                "python-build-for-publication",
                "python-build-for-development",
            ]
        super().__init__(*args, **kwargs)

    def parse_trace(self, gl_job, trace):
        result = {}

        # Base artifact URL to download file attached to job. There is
        # also an URL for the package in the package registry, but
        # unfortunately finding that one is a bit tricky: We need the
        # package ID, and because GitLab doesn't allow searching for
        # packages by version we'd need to search manually through all
        # versions to find it.
        artifact_url = (
            f"{self.project.manager.gitlab.url}/"
            f"{self.project.path_with_namespace}/"
            f"-/jobs/{gl_job.id}/artifacts/raw/dist"
        )
        result["pypi_url"] = (
            f"{self.project.manager.gitlab.api_url}/projects/"
            f"{self.project.id}/packages/pypi/simple"
        )

        # Look for "Successfully built" messages
        build_tar_gz_re = re.compile(
            r"^Successfully built.*\s([\w\d\-\.\+]+\.tar\.\wz)"
        )
        build_whl_re = re.compile(
            r"^Successfully built.*\s([\w\d\-\.\+]+\.whl)"
        )
        build_whl2_re = re.compile(
            r"^Archive is: .*/([\w\d\-\.\+]+\.whl) of type: whl"
        )
        for line in trace.split("\n"):
            match = build_tar_gz_re.match(line)
            if match is not None:
                result["tarball"] = match[1]
                result["tarball_url"] = (
                    f"{artifact_url}/{result['tarball']}?inline=false"
                )

            match = build_whl_re.match(line)
            if match is not None:
                result["wheel"] = match[1]
                result["wheel_url"] = (
                    f"{artifact_url}/{result['wheel']}?inline=false"
                )

            match = build_whl2_re.match(line)
            if match is not None:
                result["wheel"] = match[1]
                result["wheel_url"] = (
                    f"{artifact_url}/{result['wheel']}?inline=false"
                )

        # Get version from file name. Note that presently there is no
        # way for us to know whether this is actually correct.
        if "tarball" not in result and "wheel" not in result:
            logger.warning(
                '%s: Job %s does not have "Successfully built" message',
                self.name,
                gl_job.id,
            )
            return None
        match = re.fullmatch(
            r"([\w\d\-]*)-([\d\.]+([_\.\+\w\d]dev[_\.\w\d]+)"
            r"?)(\-py[\-\d\w]*\.whl|\.tar.gz)",
            result.get("tarball") or result.get("wheel"),
        )
        if not match:
            logger.warning(
                "%s: Job %d has strange build artefact name: %s",
                self.name,
                gl_job.id,
                result.get("tarball") or result.get("wheel"),
            )
            return None

        # Done, put finishing touches
        result["name"] = match[1].replace("_", "-")
        result["version"] = match[2]
        logger.info(
            "%s: Found %s version %s",
            self.name,
            result["name"],
            result["version"],
        )
        return result


class GitlabOciImageDep(GitlabTraceDep):
    """OCI image built from source code on GitLab

    Parameters:

    * ``dep_type``: Should be ``'gitlab:oci-image'``
    * ``project``: Gitlab project name (e.g.
      ``ska-telescope/sdp/ska-sdp-proccontrol``)
    * ``branch``: (List of) branch names in priority order
      (e.g. ``[ "{branch}", "main", "master" ]``)
    * ``tag``: (List of) tags in priority order (e.g. ``[ "1.2.1" ]``)
      - overrides ``branch``
    * ``job``: Job name to look for (e.g. ``[ 'oci-image-build' ]``)
    """  # noqa: 501

    @classmethod
    def dep_type_name(cls):
        return "gitlab:oci-image"

    def __init__(self, *args, **kwargs):
        if "job" not in kwargs:
            kwargs["job"] = ["oci-image-build"]
        super().__init__(*args, **kwargs)

    def parse_trace(self, gl_job, trace):
        result = {}

        regex_map = {
            "oci-image-build": r"^ociImageBuild: pushed to "
            r"([\w\d\./\-_]*):([\w\d\./\-_]*)",
            "oci-image-publish": r"^oci-do-publish: Pushing to "
            r"([\w\d\./\-_]*):([\w\d\./\-_]*)",
        }
        # Look for "pushed to" messages
        pushed_re = re.compile(regex_map[gl_job.name])
        for line in trace.split("\n"):
            match = pushed_re.match(line)
            if match is not None:
                result["path"] = match[1]
                result["registry"], result["name"] = os.path.split(
                    result["path"]
                )
                result["version"] = match[2]
                break

        if "name" not in result:
            logger.warning(
                '%s: Job %s does not have "ociImageBuild: pushed to" message',
                self.name,
                gl_job.id,
            )
            return None

        # Done, put finishing touches
        logger.info(
            "%s: Found %s version %s",
            self.name,
            result["name"],
            result["version"],
        )
        return result


class GitlabHelmChartDep(GitlabTraceDep):
    """Helm chart built from source code on GitLab

    Parameters:

    * ``dep_type``: Should be ``'gitlab:helm-chart'``
    * ``project``: Gitlab project name (e.g.
      ``ska-telescope/sdp/ska-sdp-integration``)
    * ``branch``: (List of) branch names in priority order
      (e.g. ``[ "{branch}", "main", "master" ]``)
    * ``tag``: (List of) tags in priority order (e.g. ``[ "1.2.1" ]``)
      - overrides ``branch``
    * ``job``: Job name to look for (e.g. ``[ 'helm-chart-build' ]``)
    * ``chart_name``: Name of chart (if multiple charts are published)
    * ``repository``: Repository where helm chart can be obtained from
      (optional). Only used if it cannot be determined from CI job
    """  # noqa: 501

    @classmethod
    def dep_type_name(cls):
        return "gitlab:helm-chart"

    def __init__(self, *args, **kwargs):
        if "job" not in kwargs:
            kwargs["job"] = ["helm-chart-build"]

        self.chart_name = kwargs.get("chart_name")
        if self.chart_name is not None:
            del kwargs["chart_name"]

        self._repository = kwargs.get("repository", "")

        super().__init__(*args, **kwargs)

    def parse_trace(self, gl_job, trace):
        result = {}

        regex_map = {
            "helm-chart-build": r"^({[^}]*})?helmChartBuild: ([\w\d\.\/\-_]+)-"
            r"([\d\.\/\-_]+(-[\w\d\.\/\-_]+)?)\.tgz "
            r"published to ([\w\d:\/\.\-_]+)/api/(\w+)",
            "helm-chart-publish": r"^({[^}]*})?helmChartPublish: "
            r"######### UPLOADING ([\w\d\.\/\-_]+)-([\d\.\/\-_]"
            r"+(-[\w\d\.\/\-_]+)?)\.tgz",
        }

        # Look for "pushed to" messages
        pushed_re = re.compile(regex_map[gl_job.name])
        for line in trace.split("\n"):
            match = pushed_re.match(line)
            if match is not None:
                if self.chart_name is not None and self.chart_name != match[2]:
                    continue
                result["name"] = match[2]
                result["version"] = match[3]
                if gl_job.name == "helm-chart-publish":
                    # the helm-chart-publish job doesn't provide a line
                    # that would contain both the chart name:version
                    # and the repository name. Therefor we set it with
                    # a user-defined value if provided
                    result["repository"] = self._repository
                else:
                    result["repository"] = f"{match[5]}/{match[6]}"
                break

        if "name" not in result:
            chart_message = (
                ""
                if self.chart_name is None
                else f" for chart {self.chart_name}"
            )
            logger.warning(
                "%s: Job %s does not have "
                '"helmChartBuild: published to" message %s',
                self.name,
                gl_job.id,
                chart_message,
            )
            return None

        # Done, put finishing touches
        logger.info(
            "%s: Found %s version %s",
            self.name,
            result["name"],
            result["version"],
        )
        return result


DEP_TYPES = [GitlabPythonDep, GitlabOciImageDep, GitlabHelmChartDep]

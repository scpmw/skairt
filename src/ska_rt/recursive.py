"""
Implementation of recursive behaviour for release
"""

import logging
import os
import shutil
import subprocess
import tempfile

import git
import requests
from git import GitCommandError

from .config import Config

logger = logging.getLogger(__name__)


class Recursive:
    """
    Class with functionalities to perform the necessary action
    to run a given command on packages recursively
    """

    def __init__(self, command=None):
        if not command:
            self._command = ["make", "deps-patch-release"]
        else:
            self._command = command

        # Create a temporary directory in the current working directory
        self._current_directory = os.getcwd()
        self._temp_dir = tempfile.mkdtemp(dir=self._current_directory)

        # Check if SKART_DEPS_FILE environment variable is set
        self._skart_dep_file = os.getenv("SKART_DEPS_FILE")
        if self._skart_dep_file is None:
            self._skart_dep_file = "skart.toml"

        # Set to keep track of processed repositories
        self._visited_repo = set()
        self._processed_repo = []

    @property
    def command(self):
        """Get the command to run"""
        return self._command

    @command.setter
    def command(self, cmd):
        """
        Set the command

        :param cmd: New command to run
        """
        if isinstance(cmd, str):
            self._command = cmd.split()
        self._command = cmd

    @property
    def processed_repo(self):
        """Get the processed repository list"""
        return self._processed_repo

    @property
    def current_directory(self):
        """Get the current directory"""
        return self._current_directory

    @property
    def temp_dir(self):
        """Get the temporary directory"""
        return self._temp_dir

    def delete_directory(self):
        """Delete the temporary directory with the cloned repositories"""
        if os.path.isdir(self.temp_dir):
            shutil.rmtree(self.temp_dir)
            logger.info(
                "Temporary Directory Deleted: %s",
                self.temp_dir,
            )

    def run_command(self, dep_name, path=None):
        """
        Run command

        :param dep_name: Dependency name
        :param path: Run on a specific path, default: None
        """

        try:
            logger.info(
                "Running %s command on repository %s", self.command, dep_name
            )
            if path is not None:
                directory_path = path
            else:
                directory_path = os.path.join(self.temp_dir, dep_name)
            subprocess.run(self.command, check=True, cwd=directory_path)
        except subprocess.CalledProcessError as err:
            logging.error("Error running %s: %s", self.command, err)
            raise RuntimeError(
                f"Failed to run command -'{self.command}'"
            ) from err

    def clone_repo(self, project, dep_name, branch=None):
        """
        Clone gitlab repository into a temporary directory.

        :param project: Gitlab project slug
        :param dep_name: Dependency name
        :param branch: git branch to clone; if None, or branch doesn't exist,
            it will clone from main/master
        :returns: the repository directory
        """

        url = f"https://gitlab.com/{project}.git"
        repo_dir = f"{self.temp_dir}/{dep_name}"

        if os.path.exists(repo_dir):
            logger.info(
                "Directory %s already exists. Skipping clone.",
                repo_dir,
            )
            return repo_dir

        response = requests.get(url, timeout=60)
        if response.status_code == 200:
            try:
                git.Repo.clone_from(
                    url,
                    repo_dir,
                    branch=branch,
                    depth=1,
                    multi_options=["--recurse-submodules"],
                )
            except GitCommandError:
                # Likely caused by branch not existing.
                # Try cloning without branch specified
                logger.error(
                    "Branch %s does not exist, cloning from main branch",
                    branch,
                )
                git.Repo.clone_from(
                    url,
                    repo_dir,
                    depth=1,
                    multi_options=["--recurse-submodules"],
                )
        else:
            raise RuntimeError(f"URL does not exist: {url}")

        return repo_dir

    def process_repo(self, dep_name, project, branch=None):
        """
        Process each repository by checking if there is a
        toml file and process dependencies until all of
        them are covered

        :param dep_name: Dependency name
        :param project: Gitlab project slug
        :param branch: git branch to clone; if None, or branch doesn't exist,
            it will clone from main/master
        """

        if dep_name not in self._visited_repo:
            self._visited_repo.add(dep_name)

            repo_dir = self.clone_repo(project, dep_name, branch=branch)
            toml_path = os.path.join(repo_dir, self._skart_dep_file)

            if os.path.exists(toml_path):
                dependencies = Config({"--dep-file": toml_path})
                for dependency_name, dep in dependencies.deps.items():
                    self.process_repo(
                        dependency_name, dep["project"], branch=branch
                    )

            logger.info("Visiting sub_component: %s", dep_name)
            self.run_command(dep_name)
            self._processed_repo.append(dep_name)

    def invoke(self, config_dep, branch=None):
        """
        Invokes the recursive behaviour
        Depending on the mode, either release packages or
        update dependencies from gitlab

        :param config_dep: Dependency configuration
        :param branch: git branch to clone; if None, or branch doesn't exist,
            it will clone from main/master
        """

        try:
            for dep_name, dep in config_dep.items():
                self.process_repo(dep_name, dep["project"], branch=branch)

            logger.info(
                "Ran %s command on all sub-components successfully. "
                "Executing it on current repository",
                self.command,
            )

            current_repo = os.path.basename(self.current_directory)
            self.run_command(current_repo, self.current_directory)

        finally:
            # Always delete the temporary directory with the clone repositories
            # even if any of the above commands fail
            self.delete_directory()

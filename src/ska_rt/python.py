"""
Dependency sinks for Python packages
"""

import abc
import logging
import os

import tomlkit

from .update import json_get, json_noisy_merge, json_noisy_update, json_set

logger = logging.getLogger(__name__)

# list of allowed keys in dependency definitions for
# [tool.poetry.<>.dependencies] groups in pyproject.toml
# based on:
# https://python-poetry.org/docs/dependency-specification/#path-dependencies
# we ignore "git" based dependencies, since skart will always update
# with proper version and optionally with a source
POETRY_DEP_KEYS = [
    "source",
    "extras",
    "python",
    "platform",
    "markers",
    "allow-prereleases",
]


class GenericPythonPoetrySink:
    """
    Generic sink for Poetry sinks. See subclasses for expected parameters.
    """

    @classmethod
    @abc.abstractmethod
    def sink_type_name(cls):
        """Dependency sink name, used in configuration files"""

    # pylint: disable=too-many-arguments,too-many-positional-arguments
    def __init__(
        self,
        config,
        sink_name,
        sink_type,
        dependency="{name}",
        file="pyproject.toml",
        section="tool.poetry.dependencies",
        version_only=False,
        **extra_args,
    ):
        assert sink_type == self.sink_type_name()
        config.warn_extra_args(sink_name, self.sink_type_name(), extra_args)
        self.config = config
        self.name = sink_name

        self.file = file
        self.section = section
        self.dependency = dependency
        self.version_only = version_only

    def update(self, data):
        """Set dependency properties of Python package

        Updates pyproject.toml as appropriate for dependency data passed in
        parameter.

        if version_only = True:
            do not add a new poetry source for updated dependency
        """

        # Load pyproject.toml
        with open(self.file, "rb") as toml_in:
            pyproject = tomlkit.loads(toml_in.read())

        # Get section
        sct = json_get(pyproject, self.section)

        # Do updates
        self._update_pyproject(pyproject, sct, data)

        # Write back
        self._write_data(pyproject)

    def _write_data(self, pyproject):
        os.rename(self.file, self.file + ".old")
        try:
            with open(self.file, "w", encoding="utf8") as toml_out:
                tomlkit.dump(pyproject, fp=toml_out)
            os.remove(self.file + ".old")
        except Exception:
            os.rename(self.file + ".old", self.file)
            raise

    @abc.abstractmethod
    def _update_pyproject(self, pyproject, sct, data):
        pass


class PythonPoetrySink(GenericPythonPoetrySink):
    """Update ``pyproject.toml`` for injecting Python dependency to Poetry
    (by custom source)

    Parameters:

    * ``file``: Target file path (e.g. ``"pyproject.toml"``)
    * ``section``: Dependency section to update (e.g.
       ``"tool.poetry.dependencies"``)
    * ``dependency``: Name of dependency (e.g. ``"{name}"`` to derive
      from dependency)

    Required dependency data:

    * ``name``: Package name
    * ``version``: Package version
    * ``pypi_url``: PyPI-compatible repository URL
    """  # noqa: E501

    @classmethod
    def sink_type_name(cls):
        return "python:poetry"

    def _update_pyproject(self, pyproject, sct, data):
        self.config.check_dep_data(
            self.name,
            self.sink_type_name(),
            data,
            ["name", "version", "pypi_url"],
        )

        dep = tomlkit.inline_table()
        dep_name = data["name"]

        if not self.version_only:
            dependency = self._update_sources(data, pyproject)
            dep["source"] = dependency

        dep["version"] = f"=={data['version']}"

        # add any keys that are not the ones in the list to the dependency
        sub_data = _sub_dict(data, POETRY_DEP_KEYS)
        if data:
            dep.update(sub_data)

        json_noisy_merge(
            self.name,
            pyproject,
            self.section + "." + dep_name,
            dep,
            warn_if_new=True,
        )

    def _update_sources(self, data, pyproject):
        # Get existing sources
        sources = json_get(pyproject, "tool.poetry.source")
        have_sources = sources is not None
        if not have_sources:
            sources = []

        # Add new source, if necessary
        dependency = self.dependency.format(**data)
        dep_sources = [
            (i, source)
            for i, source in enumerate(sources)
            if source["name"] == dependency
        ]

        if not dep_sources:
            logger.info(
                "%s: Adding tool.poetry.source for %s (%s)",
                self.name,
                dependency,
                data["pypi_url"],
            )
            dep_source = tomlkit.table()
            dep_source["name"] = dependency
            dep_source["url"] = data["pypi_url"]
            dep_source["priority"] = "supplemental"
            dep_source.add(tomlkit.nl())
            sources.append(dep_source)
            if not have_sources:
                json_set(pyproject, "tool.poetry.source", sources)

        else:
            dep_nr, dep_source = dep_sources[0]
            json_noisy_update(
                self.name,
                dep_source,
                "name",
                dependency,
                f"tool.poetry.source[{dep_nr}].",
            )
            json_noisy_update(
                self.name,
                dep_source,
                "url",
                data["pypi_url"],
                f"tool.poetry.source[{dep_nr}].",
            )
            json_noisy_update(
                self.name,
                dep_source,
                "priority",
                "supplemental",
                f"tool.poetry.source[{dep_nr}].",
            )
        return dependency


class PythonPoetrySinkURL(GenericPythonPoetrySink):
    """Update ``pyproject.toml`` for injecting Python dependency to Poetry
    (by URL)

    Parameters:

    * ``file``: Target file path (e.g. ``"pyproject.toml"``)
    * ``section``: Dependency section to update
      (e.g. ``"tool.poetry.dependencies"``)
    * ``dependency``: Name of dependency (e.g. ``"{name}"`` to derive
      from dependency)

    Required dependency data:

    * ``wheel_url``: URL to download wheel
    """

    @classmethod
    def sink_type_name(cls):
        return "python:poetry_url"

    def _update_pyproject(self, pyproject, sct, data):
        self.config.check_dep_data(
            self.name, self.sink_type_name(), data, ["wheel_url"]
        )

        # Set dependency
        dependency = self.dependency.format(**data)
        dep = tomlkit.inline_table()
        dep["url"] = data["wheel_url"]

        # add any keys that are not the ones in the list to the dependency
        sub_data = _sub_dict(data, POETRY_DEP_KEYS)
        dep.update(sub_data)

        json_noisy_merge(
            self.name,
            pyproject,
            self.section + "." + dependency,
            dep,
            warn_if_new=True,
        )


SINK_TYPES = [PythonPoetrySink, PythonPoetrySinkURL]


def _sub_dict(dictionary: dict, keys_to_keep: list):
    """
    Return a sub-dictionary of input dictionary with
    only keys that are in keys_to_keep if they exist.
    """
    new_dict = dictionary.copy()
    new_dict_keys = list(new_dict.keys())
    for key in new_dict_keys:
        if key not in keys_to_keep:
            new_dict.pop(key)
    return new_dict

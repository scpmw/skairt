"""
Useful routines for updating configuration files in JSON, YAML or TOML
format.
"""

import logging

from jsonpath_ng import Fields, Index
from jsonpath_ng.ext import parse as jsonpath_parse

logger = logging.getLogger(__name__)


def json_get(json, path):
    """Get a value from JSON by path expression

    :param json: JSON-like data
    :param path: JSON path to change (must identify single value)
    """

    match = jsonpath_parse(path).find(json)
    if not match:
        return None
    if len(match) > 1:
        logger.warning(
            "JSON path '%s' matched multiple paths, only using first!", path
        )
    return match[0].value


def _json_set_match(match, value, merge_existing=False):
    """
    Sets a "jsonpath" match to a particular value.

    :param match: Match returned by jsonpath
    :param value: Value to set to
    :param merge_existing: If value is a dictionary - merge with existing data
    """

    if isinstance(match.path, Fields):
        for field in match.path.fields:
            # Perform merge? Both sides must be dictionaries!
            if (
                merge_existing
                and isinstance(value, dict)
                and isinstance(match.value, dict)
            ):
                for key, val in value.items():
                    match.value[key] = val
            else:
                match.context.value[field] = value
    elif isinstance(match.path, Index):
        match.context.value[match.path.index] = value
    else:
        logger.error(
            "Internal error: Do not know how to perform "
            "update identified as %s!",
            match.path,
        )


def json_set(json, path, value, merge_existing=False):
    """Set a value in JSON by path expression

    If the path does not match, we will simplify the path until it
    matches at least one element, then create intermediate elements so
    we can set at least one value.

    :param json: JSON-like data
    :param path: JSON path to change (must identify single value)
    :param value: Value to set
    :param merge_existing: If value is a dictionary - merge with existing data
    """

    # Works *mostly*, but breaks TOML in all kinds of ways:
    # jsonpath_parse(path).update_or_create(json, value)

    expr = jsonpath_parse(path)
    while True:
        # Can match expression?
        matches = expr.find(json)
        if matches:
            # Found! Just need to set it
            for match in matches:
                _json_set_match(match, value, merge_existing)
            # Done
            return

        # Otherwise: Simplify expression
        if isinstance(expr, Fields):
            # Top level
            for field in expr.fields:
                json[field] = value
            return
        if isinstance(expr.right, Fields):
            value = {field: value for field in expr.right.fields}
            merge_existing = True
        else:
            logger.error(
                "Internal error: Do not know how to create "
                "path element identified as %s!",
                expr.right,
            )
            return
        expr = expr.left


# pylint: disable-next=too-many-arguments,too-many-positional-arguments
def _json_update_merge_final_logging(
    context, current, extra_path, path, value, warn_if_new
):
    """
    Common code in json_noisy_update and json_noisy_merge.
    This logs the final state of the update or the merge of
    "current" (the existing JSON data) and "value" (the new JSON data).
    """
    # Plain value on one side or the other? Replace entirely
    if current is None:
        if warn_if_new:
            logger.warning(
                "%s: Adding new %s%s = %s", context, extra_path, path, value
            )
        else:
            logger.info(
                "%s: Adding %s%s = %s", context, extra_path, path, value
            )
    elif value != current:
        logger.info(
            "%s: Setting %s%s = %s (old: %s)",
            context,
            extra_path,
            path,
            value,
            current,
        )


# pylint: disable-next=too-many-arguments,too-many-positional-arguments
def json_noisy_update(
    context, json, path, value, extra_path="", warn_if_new=False
):
    """
    Set a value in JSON by '.'-separated path, announce any changes in log.
    It removes any keys from the existing data if that is not present in
    the new data, when both existing and new JSON data come as dictionaries.
    (In contrast, json_noisy_merge recursively merges data in such case.)

    :param context: Context name (for logging)
    :param json: JSON structure to update
    :param path: Path in structure to update
    :param value: Value to set (new data)
    :param extra_path: Path prefix of structure (for log messages)
    :param warn_if_new: Warn if path doesn't already exist in structure
    """

    current = json_get(json, path)

    # Nested? Recurse
    if isinstance(value, dict) and isinstance(current, dict):
        for name in set(value.keys()) | set(current.keys()):
            if name not in value.keys():
                logger.warning(
                    "%s: Removing %s%s.%s", context, extra_path, path, name
                )
                del current[name]
            else:
                json_noisy_update(
                    context, json, path + "." + name, value[name], extra_path
                )
        return

    _json_update_merge_final_logging(
        context, current, extra_path, path, value, warn_if_new
    )

    json_set(json, path, value)


# pylint: disable-next=too-many-arguments,too-many-positional-arguments
def json_noisy_merge(
    context, json, path, value, extra_path="", warn_if_new=False
):
    """
    Set a value in JSON by '.'-separated path, announce any changes in log.
    It recursively merges data if both existing and new JSON data
    come as dictionaries. (In contrast, json_noisy_update will remove
    any keys from the existing data if that is not present in the new data.)

    :param context: Context name (for logging)
    :param json: JSON structure to update
    :param path: Path in structure to update
    :param value: Value to set (new data)
    :param extra_path: Path prefix of structure (for log messages)
    :param warn_if_new: Warn if path doesn't already exist in structure
    """

    current = json_get(json, path)

    # The following occurs when in pyproject.toml the dependency is provided
    # with its version only as a string (no source or any extra argument),
    # and we are using mode==release (i.e. not updating sources). If the
    # incoming updates contain only the version info, reset the JSON update
    # to return the dependency with version as a string
    # Example:
    #   current = "^0.1.1"
    #   value = {"version": "==0.2.0"}
    #   reset to -> value = "==0.2.0"
    if (
        isinstance(current, str)
        and isinstance(value, dict)
        and list(value.keys()) == ["version"]
    ):
        value = value["version"]

    # Nested? Recurse
    if isinstance(value, dict) and isinstance(current, dict):
        for name in set(value.keys()) | set(current.keys()):
            if name not in value.keys():
                # if entry in current but not in value, keep it
                # and add it to value as well
                value[name] = current[name]
            else:
                json_noisy_update(
                    context, json, path + "." + name, value[name], extra_path
                )
        return

    _json_update_merge_final_logging(
        context, current, extra_path, path, value, warn_if_new
    )

    json_set(json, path, value)

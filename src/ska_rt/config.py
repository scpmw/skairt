# pylint: disable=too-many-instance-attributes,too-many-branches
# pylint: disable=too-many-statements
"""
Configuration and top-level GitLab API handling
"""

import logging
import os
import subprocess
import sys
import time

import gitlab
import tomlkit

# Dependency & sink types
from .gitlab import DEP_TYPES as GITLAB_DEP_TYPES
from .python import SINK_TYPES as PYTHON_SINK_TYPES
from .yaml import SINK_TYPES as YAML_SINK_TYPES

DEP_TYPE_MAP = {
    dep_type.dep_type_name(): dep_type for dep_type in GITLAB_DEP_TYPES
}
SINK_TYPE_MAP = {
    sink_type.sink_type_name(): sink_type
    for sink_type in PYTHON_SINK_TYPES + YAML_SINK_TYPES
}

logger = logging.getLogger(__name__)


class Config:
    """
    Repository tooling configuration

    Used to generate configured dependency sources and sinks
    """

    # pylint: disable=too-many-locals
    def __init__(self, args, dep_type_map=None, sink_type_map=None):
        # Set properties
        wait_time = args.get("--wait")
        if wait_time is not None:
            self.pipeline_wait = float(wait_time)
        else:
            self.pipeline_wait = 0.0

        requery = args.get("--requery")
        if requery is not None:
            self.pipeline_requery = float(requery)
        else:
            self.pipeline_requery = 5.0

        self.did_auth_warning = False
        mode = args.get("--mode")
        if mode is not None:
            self.mode = mode
        else:
            self.mode = ""

        # Get current branch - first check command line, then various
        # CI environment variables, finally call git
        self.branch = args.get("--branch")
        if self.branch is None and os.getenv(
            "CI_MERGE_REQUEST_SOURCE_BRANCH_NAME"
        ):
            self.branch = os.getenv("CI_MERGE_REQUEST_SOURCE_BRANCH_NAME")
            logger.info(
                "Current branch is %s "
                "(from CI_MERGE_REQUEST_SOURCE_BRANCH_NAME)",
                self.branch,
            )
        elif self.branch is None and os.getenv("CI_COMMIT_BRANCH"):
            self.branch = os.getenv("CI_COMMIT_BRANCH")
            logger.info(
                "Current branch is %s (from CI_COMMIT_BRANCH)", self.branch
            )
        elif self.branch is None:
            code, message = subprocess.getstatusoutput(
                "git rev-parse --abbrev-ref HEAD"
            )
            if code == 0:
                self.branch = message
                logger.info("Current branch is %s (from git)", self.branch)
            else:
                self.branch = None

        # Initialise Gitlab API
        gl_config = {}
        if args.get("--gitlab-config") is not None:
            try:
                gl_config = {
                    key_arg[: key_arg.index("=")]: key_arg[
                        key_arg.index("=") + 1 :
                    ]
                    for key_arg in args["--gitlab-config"].split(",")
                }
            except ValueError as err:
                # input string doesn't contain '='
                raise ValueError(
                    "--gitlab-config needs to be a string of "
                    "'key1=value1,key2=value2' pairs."
                ) from err

        config_files = []
        if os.path.exists(".python-gitlab.cfg"):
            config_files.append(".python-gitlab.cfg")
        self.gitlab = gitlab.Gitlab.merge_config(
            gl_config, config_files=config_files
        )

        if dep_type_map is None:
            dep_type_map = list(DEP_TYPE_MAP.values())
        if sink_type_map is None:
            sink_type_map = list(SINK_TYPE_MAP.values())

        self.dep_types = {
            dep_type.dep_type_name(): dep_type for dep_type in dep_type_map
        }
        self.sink_types = {
            sink_type.sink_type_name(): sink_type
            for sink_type in sink_type_map
        }

        if args.get("--dep-config") is None:
            dep_config_env = os.getenv("SKART_DEPS_FILE")
            if dep_config_env:
                # Scenario 1: If an environment variable is set, use it
                # and ignore all other user-provided options
                dep_file = dep_config_env
                logging.info("Using environment variable %s", dep_file)
            else:
                # Scenario 2 and 3: env variable not set but --dep-file
                # is provided and if not found, the 'skart.toml' file will
                # be used
                dep_file = args["--dep-file"] or "skart.toml"
            try:
                with open(dep_file, "rb") as skart_toml:
                    config = tomlkit.load(skart_toml)
                    logging.info("Loaded configuration from %s", dep_file)
            except FileNotFoundError:
                # Scenario 3: If the specified file is not found, fallback
                # to “skart.toml”
                if dep_file != "skart.toml":
                    logging.warning(
                        "The dependency file %s cannot be found. "
                        "Falling back to 'skart.toml' as default.",
                        dep_file,
                    )
                    try:
                        with open("skart.toml", "rb") as default_toml:
                            config = tomlkit.load(default_toml)
                        logging.info("Loaded configuration from skart.toml")
                    except FileNotFoundError:
                        logging.error(
                            "Neither the specified %s nor 'skart.toml' "
                            "could be found.",
                            dep_file,
                        )
                        sys.exit(1)
        else:
            logger.info("CONFIG: %s", args["--dep-config"])
            try:
                config = tomlkit.loads(args["--dep-config"])
            except tomlkit.exceptions.UnexpectedCharError as err:
                raise ValueError(
                    "--dep-config needs to follow toml structure."
                ) from err

        self.all_deps = config.get("dep", {})

        if args.get("<dependency>") is not None:
            if args["<dependency>"] not in self.all_deps:
                logger.error(
                    "Dependency %s not defined in %s!",
                    args["<dependency>"],
                    dep_file,
                )
                sys.exit(1)
            self.deps = {
                args["<dependency>"]: self.all_deps[args["<dependency>"]]
            }
        else:
            self.deps = self.all_deps

    def make_dep(self, dep_name, dep_config):
        """
        Create dependency source

        :param dep_name: Dependency name
        :param dep_config: Dependency configuration (dep_type gives the type)
        """

        # Find dependency type
        dep_type_name = dep_config.get("dep_type")
        if dep_type_name not in self.dep_types:
            logger.error(
                "Dependency %s has unknown type %s!", dep_name, dep_type_name
            )
            logger.error(
                "Invoke with 'help' to get a list of valid dependency types!"
            )
            return None

        updated_config = dep_config.copy()
        for key, value in updated_config.get(self.mode, {}).items():
            updated_config[key] = value

        # Construct
        return self.dep_types[dep_type_name](self, dep_name, **updated_config)

    def make_sink(self, dep_name, sink_name, sink_config):
        """
        Create dependency sink

        :param dep_name: Dependency name
        :param sink_name: Dependency sink name
        :param sink_config: Sink configuration (sink_type gives the type)
        """
        # Identify sink type
        sink_type_name = sink_config.get("sink_type")
        if sink_type_name not in self.sink_types:
            logger.error(
                "Sink %s.sink.%s has unknown type %s!",
                dep_name,
                sink_name,
                sink_type_name,
            )
            logger.error(
                "Invoke with 'help' to get a list of valid sink types!"
            )
            return None

        updated_config = sink_config.copy()
        for key, value in updated_config.get(self.mode, {}).items():
            updated_config[key] = value

        # Construct
        return self.sink_types[sink_type_name](
            self, dep_name + "/" + sink_name, **updated_config
        )

    def pipeline_requery_wait(self, start_time):
        """
        Waits to requery pipeline status.

        This will requery every "pipeline_requery" seconds, and fail after
        "pipeline_wait" seconds have passed since "start_time".

        :param start_time: Time we started waiting
        :returns: Whether to continue waiting
        """
        elapsed_time = time.time() - start_time
        time_remaining = self.pipeline_wait - elapsed_time
        wait_time = min(time_remaining, self.pipeline_requery)
        logger.debug("Wait time: %s", wait_time)
        if wait_time <= 0:
            return False
        time.sleep(wait_time)
        return True

    def auth_warning(self):
        """
        Warn about missing authentication, if applicable
        (and not repeated)
        """

        if not self.authenticated() and not self.did_auth_warning:
            logger.warning(
                "Not authenticated to Gitlab API! "
                "Invoke with 'help auth' for more information."
            )
            self.did_auth_warning = True

    def authenticated(self):
        """Checks whether the Gitlab configuration has a token."""

        return (
            self.gitlab.private_token is not None
            or self.gitlab.oauth_token is not None
            or self.gitlab.job_token is not None
        )

    @staticmethod
    def warn_extra_args(context, context_type, extra_args):
        """Generate standard warning for superfluous arguments."""

        # Identify extra keys, ignoring those that have a dictionary value
        # (we assume them to likely be mode parameters)
        extra_keys = [
            key
            for key, value in extra_args.items()
            if not isinstance(value, dict)
        ]

        if extra_keys:
            logger.warning(
                "%s: Unknown parameter%s for %s: %s",
                context,
                "s" if len(extra_args) > 1 else "",
                context_type,
                ", ".join(extra_keys),
            )
            logger.warning(
                "%s: Invoke with 'help %s' to get list of parameters!",
                context,
                context_type,
            )

    @staticmethod
    def check_dep_data(context, context_type, data, expected):
        """Generate error if dependency data is missing."""

        missing = [name for name in expected if name not in data]
        if missing:
            logger.error(
                "%s: Missing dependency data for %s: %s",
                context,
                context_type,
                ", ".join(missing),
            )
            logger.error(
                "%s: Invoke with 'help %s' to get list of required data!",
                context,
                context_type,
            )
            sys.exit(0)

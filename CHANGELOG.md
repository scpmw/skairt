# Changelog

## 1.1.0

* Bugfix: `skart update` should not remove already existing keys in poetry dependency entities ([MR28](https://gitlab.com/ska-telescope/ska-rt/-/merge_requests/28))
* Fix logging in `skart invoke-recursive` ([MR28](https://gitlab.com/ska-telescope/ska-rt/-/merge_requests/28))
* Several documentation-related updates:
    - Extended documentation for multi-repository usage ([MR27](https://gitlab.com/ska-telescope/ska-rt/-/merge_requests/27))
    - Fixed sphinx errors in RTD documentation ([MR24](https://gitlab.com/ska-telescope/ska-rt/-/merge_requests/24))
    - Updaetd the CLI documentation ([MR22](https://gitlab.com/ska-telescope/ska-rt/-/merge_requests/22))
    - Added initial extended documentation ([MR20](https://gitlab.com/ska-telescope/ska-rt/-/merge_requests/20))
* Improved test coverage ([MR26](https://gitlab.com/ska-telescope/ska-rt/-/merge_requests/26))
* `skart invoke-recursive` raises error when it occurs instead of just logging it ([MR23](https://gitlab.com/ska-telescope/ska-rt/-/merge_requests/23))
* Config uses toml file from "SKART_DEPS_FILE" environment variable if it is set ([MR21](https://gitlab.com/ska-telescope/ska-rt/-/merge_requests/21))
* `skart invoke-recursive` runs the same command at starting repository as on all other
  repositories in the chain ([MR21](https://gitlab.com/ska-telescope/ska-rt/-/merge_requests/21))

## 1.0.3

* Address poetry 1.8 compatibility issues ([MR19](https://gitlab.com/ska-telescope/ska-rt/-/merge_requests/19))

## 1.0.2

* Fixed the following issues:
    - Gitlab warns about us not requesting all branches (theoretically matters in repositories with many branches)
    - Mode-specific settings for dependencies or sinks generated a warning
    - For schedules running on branches with an associated merge request, merge request pipelines were always picked up ahead of scheduled pipelines
      ([MR14](https://gitlab.com/ska-telescope/ska-rt/-/merge_requests/14))([MR16](https://gitlab.com/ska-telescope/ska-rt/-/merge_requests/16))([MR17](https://gitlab.com/ska-telescope/ska-rt/-/merge_requests/17))([MR18](https://gitlab.com/ska-telescope/ska-rt/-/merge_requests/18))


## 1.0.1

* Fully implemented recursive behaviour which allows updating dependencies automatically for a whole dependency tree ([MR9](https://gitlab.com/ska-telescope/ska-rt/-/merge_requests/9))
